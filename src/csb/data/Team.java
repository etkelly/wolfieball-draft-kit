package csb.data;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Ethan
 */
public class Team {
    private final ObservableList<Player> players;
    private final ObservableList<Player> taxiPlayers;
    private final StringProperty name;
    private final StringProperty owner;
    private final IntegerProperty dollars;
    private final IntegerProperty perPlayer;
    private final IntegerProperty playersNeeded;
    private final IntegerProperty R;
    private final IntegerProperty HR;
    private final IntegerProperty RBI;
    private final IntegerProperty SB;
    private final DoubleProperty BA;
    private final DoubleProperty W;
    private final IntegerProperty K;
    private final IntegerProperty SV;
    private final DoubleProperty ERA;
    private final DoubleProperty WHIP;
    private int countHitters;
    private int countPitchers;
    private int totalPoints;
    
    
    public Team(){
        players = FXCollections.observableArrayList();
        taxiPlayers = FXCollections.observableArrayList();
        name = new SimpleStringProperty("<Team Name>");
        owner = new SimpleStringProperty("<Team Owner>");
        dollars = new SimpleIntegerProperty(260);
        perPlayer = new SimpleIntegerProperty(11);
        playersNeeded = new SimpleIntegerProperty(23);
        R = new SimpleIntegerProperty(0);
        HR = new SimpleIntegerProperty(0);
        RBI = new SimpleIntegerProperty(0);
        SB = new SimpleIntegerProperty(0);
        K = new SimpleIntegerProperty(0);
        SV = new SimpleIntegerProperty(0);
        BA = new SimpleDoubleProperty(0);
        W = new SimpleDoubleProperty(0);
        ERA = new SimpleDoubleProperty(0);
        WHIP = new SimpleDoubleProperty(0);
        countHitters = 0;
        countPitchers = 0;
        totalPoints = 0;
    }
    public int getHittersNeeded(){
        return 14 - countHitters;
    }
    public int getPitchersNeeded(){
        return 9 - countPitchers;
    }
    public ObservableList<Player> getPlayers(){
        return players;
    }
    public void addPlayer(Player playerToAdd){
        if(countHitters + countPitchers < 23){
            players.add(playerToAdd);
        }
        else{
            taxiPlayers.add(playerToAdd);
        }
        setStats();
    }
    public int getTaxiSize(){
        return taxiPlayers.size();
    }
    public boolean hasPosition(String pos){
        int countPos = 0;
        boolean hasit = false;
        if(pos.equalsIgnoreCase("C")){
            for(Player pl: players){
                if(pl.getPosition().equalsIgnoreCase(pos)){
                    countPos++;
                }
                if(countPos == 2){
                    hasit = true;
                    break;
                }
            }
        }
        else if(pos.equalsIgnoreCase("OF")){
            for(Player pl: players){
                if(pl.getPosition().equalsIgnoreCase(pos)){
                    countPos++;
                }
                if(countPos == 5){
                    hasit = true;
                    break;
                }
            }
        }
        else if(pos.equalsIgnoreCase("P")){
            for(Player pl: players){
                if(pl.getPosition().equalsIgnoreCase(pos)){
                    countPos++;
                }
                if(countPos == 9){
                    hasit = true;
                    break;
                }
            }
        }
        else{
            for(Player pl: players){
                if(pl.getPosition().equalsIgnoreCase(pos)){
                    hasit = true;
                    break;
                }
            }
        }
        return hasit;
    }
    public ObservableList<Player> getTaxi(){
        return taxiPlayers;
    }
    public StringProperty getNameProperty(){
        return name;
    }
    public void removePlayer(Player playerToRemove){
        for(Player pl: players){
            if(pl.getFirstName().equalsIgnoreCase(playerToRemove.getFirstName()) && pl.getLastName().equalsIgnoreCase(playerToRemove.getLastName())){
                players.remove(playerToRemove);
                return;
            }
        }
        for(Player pl: taxiPlayers){
            if(pl.getFirstName().equalsIgnoreCase(playerToRemove.getFirstName()) && pl.getLastName().equalsIgnoreCase(playerToRemove.getLastName())){
                taxiPlayers.remove(playerToRemove);
                return;
            }
        }
        setStats();
    }
    public String getName(){
        return name.get();
    }
    public void setName(String initName){
        name.set(initName);
    }
    public int totalPointsProerty(){
        return totalPoints;
    }
    public void setTotalPoints(int init){
        totalPoints = init;
    }
    public int getTotalPoints(){
        return totalPoints;
    }
    public StringProperty ownerProperty(){
        return owner;
    }
    public String getOwner(){
        return owner.get();
    }
    public void setOwner(String initOwner){
       owner.set(initOwner);
    }
    public IntegerProperty dollarsProperty(){
        return dollars;
    }
    public int getDollars(){
        return dollars.get();
    }
    public void setDollars(int initDollars){
        dollars.set(initDollars);
    }
    public IntegerProperty perPlayerProperty(){
        return perPlayer;
    }
    public int getPerPlayer(){
        return perPlayer.get();
    }
    public void setPerPlayer(int init){
        perPlayer.set(init);
    }
    public IntegerProperty playersNeededProperty(){
        return playersNeeded;
    }
    public int getPlayersNeeded(){
        return playersNeeded.get();
    }
    public void setPlayersNeeded(int init){
        playersNeeded.set(init);
    }
    public IntegerProperty RProperty(){
        return R;
    }
    public int getR(){
        return R.get();
    }
    public void setR(int init){
        R.set(init);
    }
    public IntegerProperty HRProperty(){
        return HR;
    }
    public int getHR(){
        return HR.get();
    }
    public void setHR(int init){
        HR.set(init);
    }
    public IntegerProperty RBIProperty(){
        return RBI;
    }
    public int getRBI(){
        return RBI.get();
    }
    public void setRBI(int init){
        RBI.set(init);
    }
    public IntegerProperty SBProperty(){
        return SB;
    }
    public int getSB(){
        return SB.get();
    }
    public void setSB(int init){
        SB.set(init);
    }
    public IntegerProperty KProperty(){
        return K;
    }
    public int getK(){
        return K.get();
    }
    public void setK(int init){
        K.set(init);
    }
    public IntegerProperty SVProperty(){
        return SV;
    }
    public int getSV(){
        return SV.get();
    }
    public void setSV(int init){
        SV.set(init);
    }
    public DoubleProperty BAProperty(){
        return BA;
    }
    public Double getBA(){
        return BA.get();
    }
    public void setBA(Double init){
        BA.set(init);
    }
    public DoubleProperty WProperty(){
        return W;
    }
    public Double getW(){
        return W.get();
    }
    public void setW(Double init){
        W.set(init);
    }
    public DoubleProperty ERAProperty(){
        return ERA;
    }
    public Double getERA(){
        return ERA.get();
    }
    public void setERA(Double init){
        ERA.set(init);
    }
    public DoubleProperty WHIPProperty(){
        return WHIP;
    }
    public Double getWHIP(){
        return WHIP.get();
    }
    public void setWHIP(Double init){
        WHIP.set(init);
    }
    
    /*public boolean needsPosition(String position){
        boolean pos = true;
        
        
        return pos;
    }*/
    public void setStats(){
        countPitchers = 0;
        countHitters = 0;
        dollars.set(260);
        int RSum = 0;
        int HRSum = 0;
        int RBISum = 0;
        int SBSum = 0;
        int SVSum = 0;
        int KSum = 0;
        double BASum = 0;
        double WSum = 0;
        double ERASum = 0;
        double WHIPSum = 0;
        for(Player pl: players){
            if(pl.getPosition().equalsIgnoreCase("P")){
                countPitchers++;
                SVSum += pl.getHRSV();
                KSum += pl.getRBIK();
                ERASum += pl.getSBERA();
                WHIPSum += pl.getBAWHIP();
                WSum += pl.getRW();
            }
            else{
                countHitters++;
                RSum += pl.getRW();
                HRSum += pl.getHRSV();
                RBISum += pl.getRBIK();
                SBSum += pl.getSBERA();
                BASum += pl.getBAWHIP();
                
            }
            this.setDollars(dollars.get() - pl.getSalary());
        }
        this.setR(RSum);
        this.setHR(HRSum);
        this.setRBI(RBISum);
        this.setSB(SBSum);
        this.setSV(SVSum);
        this.setK(KSum);
        this.setBA(round(BASum / countHitters, 3));
        this.setW(WSum);
        this.setERA(round(ERASum / countPitchers, 2));
        this.setWHIP(round(WHIPSum / countPitchers, 2));
        this.setPlayersNeeded(23- (countHitters + countPitchers));
        if(playersNeeded.get() != 0){
            this.setPerPlayer(dollars.get() / playersNeeded.get());
        }
        else this.setPerPlayer(0);
    }
    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
}
}
