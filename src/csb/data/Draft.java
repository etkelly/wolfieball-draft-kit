/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.data;

import java.util.Collections;
import java.util.Comparator;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Ethan
 */
public class Draft {
    private final ObservableList<Team> teams;
    private final StringProperty name;
    private int transactionNumber = 1;
    public Draft(){
        name = new SimpleStringProperty("<Draft Name>");
        teams = FXCollections.observableArrayList();
    }
    public ObservableList<Team> getTeams(){
        return teams;
    }
    public void addTeam(Team teamToAdd){
        teams.add(teamToAdd);
    }
    public void removeTeam(Team teamToRemove){
        teams.remove(teamToRemove);
    }
    public String getName(){
        return name.get();
    }
    public void setName(String initName){
        name.set(initName);
    }
    public int getTransactionNumber(){
        return transactionNumber;
    }
    public void setTransactionNumber(int init){
        transactionNumber = init;
    }
    public void giveTeamPoints(){
        Collections.sort(teams, (Team o1, Team o2) -> {
            if(o1.getR() > o2.getR()){
                return 1;
            }
            else if(o1.getR() < o2.getR()){
                return -1;
            }
            else return 0;
        });
        for(int i = 0; i < teams.size(); i++){
            Team te = teams.get(i);
            te.setTotalPoints(i + 1);
        }
        
        Collections.sort(teams, (Team o1, Team o2) -> {
            if(o1.getHR() > o2.getHR()){
                return 1;
            }
            else if(o1.getHR() < o2.getHR()){
                return -1;
            }
            else return 0;
        });
        for(int i = 0; i < teams.size(); i++){
            Team te = teams.get(i);
            te.setTotalPoints(te.getTotalPoints() + i + 1);
        }
        
        Collections.sort(teams, (Team o1, Team o2) -> {
            if(o1.getRBI() > o2.getRBI()){
                return 1;
            }
            else if(o1.getRBI() < o2.getRBI()){
                return -1;
            }
            else return 0;
        });
        for(int i = 0; i < teams.size(); i++){
            Team te = teams.get(i);
            te.setTotalPoints(te.getTotalPoints() + i + 1);
        }
        
        Collections.sort(teams, (Team o1, Team o2) -> {
            if(o1.getSB() > o2.getSB()){
                return 1;
            }
            else if(o1.getSB() < o2.getSB()){
                return -1;
            }
            else return 0;
        });
        for(int i = 0; i < teams.size(); i++){
            Team te = teams.get(i);
            te.setTotalPoints(te.getTotalPoints() + i + 1);
        }
        
        Collections.sort(teams, (Team o1, Team o2) -> {
            if(o1.getBA() > o2.getBA()){
                return 1;
            }
            else if(o1.getBA() < o2.getBA()){
                return -1;
            }
            else return 0;
        });
        for(int i = 0; i < teams.size(); i++){
            Team te = teams.get(i);
            te.setTotalPoints(te.getTotalPoints() + i + 1);
        }
        
        Collections.sort(teams, (Team o1, Team o2) -> {
            if(o1.getW() > o2.getW()){
                return 1;
            }
            else if(o1.getW() < o2.getW()){
                return -1;
            }
            else return 0;
        });
        for(int i = 0; i < teams.size(); i++){
            Team te = teams.get(i);
            te.setTotalPoints(te.getTotalPoints() + i + 1);
        }
        
        Collections.sort(teams, (Team o1, Team o2) -> {
            if(o1.getSV() > o2.getSV()){
                return 1;
            }
            else if(o1.getSV() < o2.getSV()){
                return -1;
            }
            else return 0;
        });
        for(int i = 0; i < teams.size(); i++){
            Team te = teams.get(i);
            te.setTotalPoints(te.getTotalPoints() + i + 1);
        }
        
        Collections.sort(teams, (Team o1, Team o2) -> {
            if(o1.getK() > o2.getK()){
                return 1;
            }
            else if(o1.getK() < o2.getK()){
                return -1;
            }
            else return 0;
        });
        for(int i = 0; i < teams.size(); i++){
            Team te = teams.get(i);
            te.setTotalPoints(te.getTotalPoints() + i + 1);
        }
        
        Collections.sort(teams, (Team o1, Team o2) -> {
            if(o1.getERA() > o2.getERA()){
                return -1;
            }
            else if(o1.getERA() < o2.getERA()){
                return 1;
            }
            else return 0;
        });
        for(int i = 0; i < teams.size(); i++){
            Team te = teams.get(i);
            te.setTotalPoints(te.getTotalPoints() + i + 1);
        }
        
        Collections.sort(teams, (Team o1, Team o2) -> {
            if(o1.getWHIP() > o2.getWHIP()){
                return -1;
            }
            else if(o1.getWHIP() < o2.getWHIP()){
                return 1;
            }
            else return 0;
        });
        for(int i = 0; i < teams.size(); i++){
            Team te = teams.get(i);
            te.setTotalPoints(te.getTotalPoints() + i + 1);
        }
    }
    public void givePlayerValues(ObservableList<Player> players){
        if(teams.size() >= 1){
        double moneyLeft = 0;
        double hittersNeeded = 0;
        double pitchersNeeded = 0;
        double meanHitterSalary = 0;
        double meanPitcherSalary = 0;
        for(Team te: teams){
            hittersNeeded += te.getHittersNeeded();
            pitchersNeeded += te.getPitchersNeeded();
            if((te.getHittersNeeded() + te.getPitchersNeeded() != 0) || (te.getTaxiSize() < 8)){
                moneyLeft += te.getDollars();
            }
        }
        ObservableList<Player> hitters = FXCollections.observableArrayList();
        ObservableList<Player> pitchers = FXCollections.observableArrayList();
        for(Player pl: players){
            if(pl.getPositions().contains("P")){
                pitchers.add(pl);
            }
            else hitters.add(pl);
        }
        // RANK BY HR/SV
        Collections.sort(hitters, (Player o1, Player o2) -> {
            if(o1.getHRSV() > o2.getHRSV()){
                return -1;
            }
            else if(o1.getHRSV() < o2.getHRSV()){
                return 1;
            }
            else return 0;
        });
        Collections.sort(pitchers, (Player o1, Player o2) -> {
            if(o1.getHRSV() > o2.getHRSV()){
                return -1;
            }
            else if(o1.getHRSV() < o2.getHRSV()){
                return 1;
            }
            else return 0;
        });
        for(int i = 0; i < hitters.size(); i++){
            hitters.get(i).setHRSVRank(i + 1);
        }
        for(int i = 0; i < pitchers.size(); i++){
            pitchers.get(i).setHRSVRank(i + 1);
        }
        // RANK BY RUNS/WINS
        Collections.sort(hitters, (Player o1, Player o2) -> {
            if(o1.getRW() > o2.getRW()){
                return -1;
            }
            else if(o1.getRW() < o2.getRW()){
                return 1;
            }
            else return 0;
        });
        Collections.sort(pitchers, (Player o1, Player o2) -> {
            if(o1.getRW() > o2.getRW()){
                return -1;
            }
            else if(o1.getRW() < o2.getRW()){
                return 1;
            }
            else return 0;
        });
        for(int i = 0; i < hitters.size(); i++){
            hitters.get(i).setRWRank(i + 1);
        }
        for(int i = 0; i < pitchers.size(); i++){
            pitchers.get(i).setRWRank(i + 1);
        }
        // RANK BY RBI/K
        Collections.sort(hitters, (Player o1, Player o2) -> {
            if(o1.getRBIK() > o2.getRBIK()){
                return -1;
            }
            else if(o1.getRBIK() < o2.getRBIK()){
                return 1;
            }
            else return 0;
        });
        Collections.sort(pitchers, (Player o1, Player o2) -> {
            if(o1.getRBIK() > o2.getRBIK()){
                return -1;
            }
            else if(o1.getRBIK() < o2.getRBIK()){
                return 1;
            }
            else return 0;
        });
        for(int i = 0; i < hitters.size(); i++){
            hitters.get(i).setRBIKRank(i + 1);
        }
        for(int i = 0; i < pitchers.size(); i++){
            pitchers.get(i).setRBIKRank(i + 1);
        }
        // RANK BY SB/ERA
        Collections.sort(hitters, (Player o1, Player o2) -> {
            if(o1.getSBERA() > o2.getSBERA()){
                return -1;
            }
            else if(o1.getSBERA() < o2.getSBERA()){
                return 1;
            }
            else return 0;
        });
        Collections.sort(pitchers, (Player o1, Player o2) -> {
            if(o1.getSBERA() > o2.getSBERA()){
                return 1;
            }
            else if(o1.getSBERA() < o2.getSBERA()){
                return -1;
            }
            else return 0;
        });
        for(int i = 0; i < hitters.size(); i++){
            hitters.get(i).setSBERARank(i + 1);
        }
        for(int i = 0; i < pitchers.size(); i++){
            pitchers.get(i).setSBERARank(i + 1);
        }
        // RANK BY BA/WHIP
        Collections.sort(hitters, (Player o1, Player o2) -> {
            if(o1.getBAWHIP() > o2.getBAWHIP()){
                return -1;
            }
            else if(o1.getBAWHIP() < o2.getBAWHIP()){
                return 1;
            }
            else return 0;
        });
        Collections.sort(pitchers, (Player o1, Player o2) -> {
            if(o1.getBAWHIP() > o2.getBAWHIP()){
                return 1;
            }
            else if(o1.getBAWHIP() < o2.getBAWHIP()){
                return -1;
            }
            else return 0;
        });
        for(int i = 0; i < hitters.size(); i++){
            hitters.get(i).setBAWHIPRank(i + 1);
            hitters.get(i).setDraftRank();
        }
        for(int i = 0; i < pitchers.size(); i++){
            pitchers.get(i).setBAWHIPRank(i + 1);
            pitchers.get(i).setDraftRank();
        }
        
        Collections.sort(hitters, (Player o1, Player o2) -> {
            if(o1.getDraftRank() > o2.getDraftRank()){
                return 1;
            }
            else if(o1.getDraftRank() < o2.getDraftRank()){
                return -1;
            }
            else return 0;
        });
        Collections.sort(pitchers, (Player o1, Player o2) -> {
            if(o1.getDraftRank() > o2.getDraftRank()){
                return 1;
            }
            else if(o1.getDraftRank() < o2.getDraftRank()){
                return -1;
            }
            else return 0;
        });
        meanHitterSalary = (moneyLeft / (2 * hittersNeeded));
        meanPitcherSalary = (moneyLeft / (2 * pitchersNeeded));
        for(int i = 0; i < hitters.size(); i++){
            Player pl = hitters.get(i);
            pl.setBAWHIPS(round(((meanHitterSalary)*((hittersNeeded*2) / (i + 1))), 3));
        }
        for(int i = 0; i < pitchers.size(); i++){
            Player pl = pitchers.get(i);
            pl.setBAWHIPS(round(((meanPitcherSalary)*((pitchersNeeded*2) / (i + 1))), 3));
        }
        }
    }
    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
}
    
    
    public Player pickBestPlayerWithPos(ObservableList<Player> players, String position){
        ObservableList<Player> playerWithPos = FXCollections.observableArrayList();
        Player bestPlayer = new Player();
        bestPlayer.setBAWHIPS(1);
        for(Player pl: players){
            if(pl.getPositions().contains(position)){
                playerWithPos.add(pl);
            }
        }
        for(Player pl: playerWithPos){
            if(pl.getBAWHIPS() >= bestPlayer.getBAWHIPS()){
                bestPlayer = pl;
            }
        }
        return bestPlayer;
    }
    public Player pickBestTaxiPlayer(ObservableList<Player> players){
        int r = (int)(Math.random()*players.size());
        return players.get(r);
    }
}
