/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.data;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.StringPropertyBase;

public class Player{
    final StringProperty firstName;
    final StringProperty lastName;  
    final StringProperty proTeam;
    final StringProperty positions;
    final IntegerProperty birth;
    final IntegerProperty RW;
    final IntegerProperty HRSV;
    final IntegerProperty RBIK;
    final IntegerProperty salary;
    final DoubleProperty SBERA;
    final DoubleProperty BAWHIP;
    final DoubleProperty BAWHIPS;
    final DoubleProperty value;
    final StringProperty notes;
    final StringProperty draftTeam;
    final StringProperty country;
    StringProperty position;
    final IntegerProperty realPosition;
    final StringProperty contract;
    final IntegerProperty transactionNumber;
    private int HRSVRank;
    private int RWRank;
    private int RBIKRank;
    private int SBERARank;
    private int BAWHIPRank;
    private int draftRank;
    
    
    public Player(){
        firstName = new SimpleStringProperty("");
        lastName = new SimpleStringProperty("");
        proTeam = new SimpleStringProperty("");
        positions = new SimpleStringProperty("");
        birth = new SimpleIntegerProperty();
        RW = new SimpleIntegerProperty();
        HRSV = new SimpleIntegerProperty();
        RBIK = new SimpleIntegerProperty();
        salary = new SimpleIntegerProperty(1);
        SBERA = new SimpleDoubleProperty();
        BAWHIP = new SimpleDoubleProperty();
        BAWHIPS = new SimpleDoubleProperty();
        value = new SimpleDoubleProperty();
        notes = new SimpleStringProperty("");
        draftTeam = new SimpleStringProperty("FREE_AGENTS");
        country = new SimpleStringProperty("");
        position = new SimpleStringProperty("");
        realPosition = new SimpleIntegerProperty();
        contract = new SimpleStringProperty("");
        transactionNumber = new SimpleIntegerProperty(0);
        HRSVRank = 0;
        RWRank = 0;
        RBIKRank = 0;
        SBERARank = 0;
        BAWHIPRank = 0;
        draftRank = 0;
    }
    
    public int getDraftRank(){
        return draftRank;
    }
    public void setDraftRank(){
        draftRank = ((HRSVRank + RWRank + RBIKRank + SBERARank + BAWHIPRank) / 5);
    }
    public int getHRSVRank(){
        return HRSVRank;
    }
    public void setHRSVRank(int i){
        HRSVRank = i;
    }
    public int getRWRank(){
        return RWRank;
    }
    public void setRWRank(int i){
        RWRank = i;
    }
    public int getRBIKRank(){
        return RBIKRank;
    }
    public void setRBIKRank(int i){
        RBIKRank = i;
    }
    public int getSBERARank(){
        return SBERARank;
    }
    public void setSBERARank(int i){
        SBERARank = i;
    }
    public int getBAWHIPRank(){
        return BAWHIPRank;
    }
    public void setBAWHIPRank(int i){
        BAWHIPRank = i;
    }
    public String getFirstName(){
        return firstName.get();
    }
    public void setFirstName(String s){
        firstName.set(s);
    }
    public int getTransactionNumber(){
        return transactionNumber.get();
    }
    public void setTransactionNumber(int tr){
        transactionNumber.set(tr);
    }
    public int getRealPosition(){
        return realPosition.get();
    }
    public void setRealPosition(int s){
        realPosition.set(s);
    }
    public String getPosition(){
        return position.get();
    }
    public SimpleStringProperty position(){
        return (SimpleStringProperty)position;
    }
    public String getContract(){
        return contract.get();
    }
    public StringProperty Contract(){
        return contract;
    }
    public void setContract(String s){
        contract.set(s);
    }
    public void setPosition(String s){
        position.set(s);
    }
    public void setTeamPositionProperty(SimpleStringProperty s){
        position = s;
    }
    public String getCountry(){
        return country.get();
    }
    public void setCountry(String s){
        country.set(s);
    }
    public String getDraftTeam(){
        return draftTeam.get();
    }
    public void setDraftTeam(String s){
        draftTeam.set(s);
    }
    public StringProperty firstNameProperty(){
        return firstName;
    }
    public String getLastName(){
        return lastName.get();
    }
    public void setLastName(String s){
        lastName.set(s);
    }
    public StringProperty lastNameProperty(){
        return lastName;
    }
    public String getProTeam(){
        return proTeam.get();
    }
    public void setProTeam(String s){
        proTeam.set(s);
    }
    public StringProperty proTeamProperty(){
        return proTeam;
    }
    public String getPositions(){
        return positions.get();
    }
    public void setPositions(String s){
        positions.set(s);
    }
    public StringProperty positionsProperty(){
        return positions;
    }
    public int getBirth(){
        return birth.get();
    }
    public void setBirth(int s){
        birth.set(s);
    }
    public IntegerProperty birthProperty(){
        return birth;
    }
    public int getSalary(){
        return salary.get();
    }
    public void setSalary(int s){
        salary.set(s);
    }
    public IntegerProperty salaryProperty(){
        return salary;
    }
    public int getRW(){
        return RW.get();
    }
    public void setRW(int s){
        RW.set(s);
    }
    public IntegerProperty RWProperty(){
        return RW;
    }
    public int getHRSV(){
        return HRSV.get();
    }
    public void setHRSV(int s){
        HRSV.set(s);
    }
    public IntegerProperty HRSVProperty(){
        return HRSV;
    }
    public int getRBIK(){
        return RBIK.get();
    }
    public void setRBIK(int s){
        RBIK.set(s);
    }
    public IntegerProperty RBIKProperty(){
        return RBIK;
    }
    public double getSBERA(){
        return SBERA.get();
    }
    public void setSBERA(double s){
        SBERA.set(round(s, 2));
    }
    public DoubleProperty SBERAProperty(){
        return SBERA;
    }
    public double getBAWHIP(){
        return BAWHIP.get();
    }
    public void setBAWHIP(double s){
        if(this.getPositions().contains("P")){
            BAWHIP.set(round(s, 2));
        }
        else{
            BAWHIP.set(round(s, 3));
        }
    }
    public DoubleProperty BAWHIPProperty(){
        return BAWHIP;
    }
    public double getBAWHIPS(){
        return BAWHIPS.get();
    }
    public void setBAWHIPS(double s){
        BAWHIPS.set(s);
    }
    public DoubleProperty BAWHIPSProperty(){
        return BAWHIPS;
    }
    public double getValue(){
        return value.get();
    }
    public void setValue(double s){
        value.set(s);
    }
    public DoubleProperty valueProperty(){
        return value;
    }
    public String getNotes(){
        return notes.get();
    }
    public void setNotes(String s){
        notes.set(s);
    }
    public StringProperty notesProperty(){
        return notes;
    }
    
     public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
}
    
}
