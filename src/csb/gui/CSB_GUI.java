package csb.gui;

import static csb.CSB_StartupConstants.*;
import csb.CSB_PropertyType;
import csb.controller.CourseEditController;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.DraftDataView;
import csb.data.CoursePage;
import csb.controller.FileController;
import csb.controller.PlayerEditController;
import csb.controller.ScheduleEditController;
import csb.controller.TeamEditController;
import csb.data.Player;
import csb.data.Team;
import csb.data.Draft;
import csb.file.CourseFileManager;
import csb.file.CourseSiteExporter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import properties_manager.PropertiesManager;

/**
 * This class provides the Graphical User Interface for this application,
 * managing all the UI components for editing a Draft and exporting it to a
 * site.
 *
 * @author Ethan Kelly
 */
public class CSB_GUI implements DraftDataView {

    

    //TO BE MOVED TO A DRAFTFILEMANAGER, ONCE WE IMPLEMENT DRAFTS
    String JSON_HITTERS = "Hitters";
    String JSON_PITCHERS = "Pitchers";
    String HITTERS_PATH = PATH_PLAYERS + "/Hitters.json";
    String PITCHERS_PATH = PATH_PLAYERS + "/Pitchers.json";
    String JSON_PLAYER_FIRST = "FIRST_NAME";
    String JSON_COUNTRY = "NATION_OF_BIRTH";
    String JSON_PLAYER_LAST = "LAST_NAME";
    String JSON_TEAM = "TEAM";
    String JSON_POSITION = "QP";
    String JSON_ATBAT = "AB";
    String JSON_RUNS = "R";
    String JSON_WINS = "W";
    String JSON_HITS = "H";
    String JSON_HOMERUNS = "HR";
    String JSON_SAVES = "SV";
    String JSON_RBI = "RBI";
    String JSON_STRIKES = "K";
    String JSON_INNINGS = "IP";
    String JSON_ER = "ER";
    String JSON_BB = "BB";
    String JSON_STOLEN_BASE = "SB";
    String JSON_NOTES = "NOTES";
    String JSON_BIRTH = "YEAR_OF_BIRTH";
    
    boolean runThread;
    Thread thread;
    Draft draft;
    Label PlayersHeadingLabel;
    Label TeamsHeadingLabel;
    Label DraftHeadingLabel;
    Label StandingsHeadingLabel;
    Label MLBHeadingLabel;
    boolean playersActivated;
    boolean teamsActivated;
    boolean draftActivated;
    boolean standingsActivated;
    boolean MLBActivated;
    boolean all = true;
    boolean C = false;
    boolean B1 = false;
    boolean CI = false;
    boolean B3 = false;
    boolean B2 = false;
    boolean MI = false;
    boolean SS = false;
    boolean OF = false;
    boolean U = false;  
    boolean P = false;
    FlowPane screenSwitchPane;
    Button playersButton;
    Button teamsButton;
    Button draftButton;
    Button standingsButton;
    Button MLBButton;
    ScrollPane playersScrollPane;
    VBox playersPane;
    ScrollPane teamsScrollPane;
    VBox teamsPane;
    ScrollPane draftScrollPane;
    VBox draftPane;
    ScrollPane standingsScrollPane;
    VBox standingsPane;
    ScrollPane MLBScrollPane;
    VBox MLBPane;
    Label searchLabel;
    TextField searchField;
    Button addPlayerButton;
    Button removePlayerButton;
    HBox playersToolbar;
    TableView<Player> playersTable;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    TableColumn teamColumn;
    TableColumn positionColumn;
    TableColumn birthColumn;
    TableColumn RWColumn;
    TableColumn HRSVColumn;
    TableColumn RBIKColumn;
    TableColumn SBERAColumn;
    TableColumn BAWHIPColumn;
    TableColumn valueColumn;
    TableColumn notesColumn;
    RadioButton allButton;
    RadioButton CButton;
    RadioButton B1Button;
    RadioButton CIButton;
    RadioButton B3Button;
    RadioButton B2Button;
    RadioButton MIButton;
    RadioButton SSButton;
    RadioButton OFButton;
    RadioButton UButton;
    RadioButton PButton;
    ToggleGroup group;
    HBox hbRadio;
    ObservableList<Player> players;
    ObservableList<Player> visiblePlayers;
    String TextFieldContent = "";
    
    VBox startingLineupBox;
    Label startingLineupLabel;
    TableView startingLineup;
    VBox taxiSquadBox;
    Label taxiSquadLabel;
    TableView taxiSquad;
    GridPane teamOpsPane;
    Label dNameLabel;
    TextField dNameTextField;
    Label teamSelectLabel;
    ComboBox teamSelectComboBox;
    Button addTeamButton;
    Button removeTeamButton;
    Button editTeamButton;
    TableColumn sFirstNameColumn;
    TableColumn sLastNameColumn;
    TableColumn sTeamColumn;
    TableColumn sPositionColumn;
    TableColumn sRWColumn;
    TableColumn sHRSVColumn;
    TableColumn sRBIKColumn;
    TableColumn sSBERAColumn;
    TableColumn sBAWHIPColumn;
    TableColumn sValueColumn;
    TableColumn sContractColumn;
    TableColumn sSalaryColumn;
    TableColumn sActualPositionColumn;
    TableColumn tFirstNameColumn;
    TableColumn tLastNameColumn;
    TableColumn tTeamColumn;
    TableColumn tPositionColumn;
    TableColumn tRWColumn;
    TableColumn tHRSVColumn;
    TableColumn tRBIKColumn;
    TableColumn tSBERAColumn;
    TableColumn tBAWHIPColumn;
    TableColumn tValueColumn;
    TableColumn tContractColumn;
    TableColumn tSalaryColumn;
    TableColumn tActualPositionColumn;
    
    
    Label proTeamLabel;
    ComboBox proTeamComboBox;
    TableView<Player> MLBTeamsTable;
    TableColumn MLBFirst;
    TableColumn MLBLast;
    TableColumn MLBPositions;
    ObservableList<Player>MLBTeamList;
    
    TableView<Team> fantasyTeamsTable;
    TableColumn teamNameColumn;
    TableColumn teamPlayersNeededColumn;
    TableColumn teamDollarsColumn;
    TableColumn teamPerPlayerColumn;
    TableColumn teamRColumn;
    TableColumn teamHRColumn;
    TableColumn teamRBIColumn;
    TableColumn teamSBColumn;
    TableColumn teamBAColumn;
    TableColumn teamWColumn;
    TableColumn teamSVColumn;
    TableColumn teamKColumn;
    TableColumn teamERAColumn;
    TableColumn teamWHIPColumn;
    TableColumn teamPointsColumn;
    
    ObservableList<Player> draftOrder;
    TableView<Player> draftList;
    Button pickButton;
    Button autoPickButton;
    Button pauseButton;
    TableColumn pickColumn;
    TableColumn draftFNameColumn;
    TableColumn draftLNameColumn;
    TableColumn draftTeamNameColumn;
    TableColumn draftContractColumn;
    TableColumn draftSalaryColumn;
    GridPane draftOpsPane;
    
    static final String CteamNameColumn = "Team Name";
    static final String CteamPlayersNeededColumn = "Players Needed";
    static final String CteamDollarsColumn = "$ Left";
    static final String CteamPerPlayerColumn = "$ PP";
    static final String CteamRColumn = "R";
    static final String CteamHRColumn = "HR";
    static final String CteamRBIColumn = "RBI";
    static final String CteamSBColumn = "SB";
    static final String CteamBAColumn = "BA";
    static final String CteamWColumn = "W";
    static final String CteamSVColumn = "SV";
    static final String CteamKColumn = "K";
    static final String CteamNameERAColumn = "ERA";
    static final String CteamWHIPColumn = "WHIP";
    static final String CteamPointsColumn = "Total Points";
    
    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES
    
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "csb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    // THIS MANAGES ALL OF THE APPLICATION'S DATA
    CourseDataManager dataManager;

    // THIS MANAGES COURSE FILE I/O
    CourseFileManager courseFileManager;

    // THIS MANAGES EXPORTING OUR SITE PAGES
    CourseSiteExporter siteExporter;

    // THIS HANDLES INTERACTIONS WITH FILE-RELATED CONTROLS
    FileController fileController;

    // THIS HANDLES INTERACTIONS WITH COURSE INFO CONTROLS
    CourseEditController courseController;
    
    // THIS HANDLES REQUESTS TO ADD OR EDIT SCHEDULE STUFF
    ScheduleEditController scheduleController;

    // REQUESTS TO ADD/EDIT PLAYERS
    PlayerEditController playerController;
    // REQUESTS TO ADD/EDIT TEAMS
    TeamEditController teamController;
    // THIS IS THE APPLICATION WINDOW
    Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane csbPane;
    
    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newCourseButton;
    Button loadCourseButton;
    Button saveCourseButton;
    Button exportSiteButton;
    Button exitButton;

    
    
    // AND TABLE COLUMNS
    static final String COL_FIRST = "First";
    static final String COL_LAST = "Last";
    static final String COL_TEAM = "Pro Team";
    static final String COL_POSITIONS = "Positions";
    static final String COL_BIRTH = "Year of Birth";
    static final String COL_RW = "R/W";
    static final String COL_HRSV = "HR/SV";
    static final String COL_RBIK = "RBI/K";
    static final String COL_SBERA = "SB/ERA";
    static final String COL_BAWHIP = "BA/WHIP";
    static final String COL_VALUE = "Estimated Value";
    static final String COL_NOTES = "Notes";
    static final String COL_CONTRACT = "Contract";
    static final String COL_PROTEAM = "Pro Team";
    static final String COL_SALARY = "Salary";
    static final String COL_POSITION = "Position";
    
    
    // HERE ARE OUR DIALOGS
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    ProgressDialog progressDialog;
    
    /**
     * Constructor for making this GUI, note that it does not initialize the UI
     * controls. To do that, call initGUI.
     *
     * @param initPrimaryStage Window inside which the GUI will be displayed.
     */
    public CSB_GUI(Stage initPrimaryStage) {
        this.runThread = true;
        primaryStage = initPrimaryStage;
    }

    /**
     * Accessor method for the data manager.
     *
     * @return The CourseDataManager used by this UI.
     */
    public CourseDataManager getDataManager() {
        return dataManager;
    }

    /**
     * Accessor method for the file controller.
     *
     * @return The FileController used by this UI.
     */
    public FileController getFileController() {
        return fileController;
    }

    /**
     * Accessor method for the course file manager.
     *
     * @return The CourseFileManager used by this UI.
     */
    public CourseFileManager getCourseFileManager() {
        return courseFileManager;
    }

    /**
     * Accessor method for the site exporter.
     *
     * @return The CourseSiteExporter used by this UI.
     */
    public CourseSiteExporter getSiteExporter() {
        return siteExporter;
    }

    /**
     * Accessor method for the window (i.e. stage).
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Stage getWindow() {
        return primaryStage;
    }
    
    public MessageDialog getMessageDialog() {
        return messageDialog;
    }
    
    public YesNoCancelDialog getYesNoCancelDialog() {
        return yesNoCancelDialog;
    }

    /**
     * Mutator method for the data manager.
     *
     * @param initDataManager The CourseDataManager to be used by this UI.
     */
    public void setDataManager(CourseDataManager initDataManager) {
        dataManager = initDataManager;
    }

    /**
     * Mutator method for the course file manager.
     *
     * @param initCourseFileManager The CourseFileManager to be used by this UI.
     */
    public void setCourseFileManager(CourseFileManager initCourseFileManager) {
        courseFileManager = initCourseFileManager;
    }

    /**
     * Mutator method for the site exporter.
     *
     * @param initSiteExporter The CourseSiteExporter to be used by this UI.
     */
    public void setSiteExporter(CourseSiteExporter initSiteExporter) {
        siteExporter = initSiteExporter;
    }

    /**
     * This method fully initializes the user interface for use.
     *
     * @param windowTitle The text to appear in the UI window's title bar.
     * @param subjects The list of subjects to choose from.
     * @throws IOException Thrown if any initialization files fail to load.
     */
    public void initGUI(String windowTitle, ArrayList<String> subjects) throws IOException {

        draft = this.getDataManager().getDraft();        
        // INIT THE DIALOGS
        initDialogs();
        
        // INIT THE TOOLBAR
        initFileToolbar();
        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        //initWorkspace(subjects);

        initPlayersScreen();
        initFantasyTeams();
        initStandingsScreen();
        initMLBTeamScreen();
        initDraftScreen();
        initScreenSwitchToolbar();
        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow(windowTitle);
    }

    public void initScreenSwitchToolbar(){
        screenSwitchPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        teamsButton = initChildButton(screenSwitchPane, CSB_PropertyType.TEAMS_ICON, CSB_PropertyType.TEAMS_TOOLTIP, true);
        playersButton = initChildButton(screenSwitchPane, CSB_PropertyType.PLAYERS_ICON, CSB_PropertyType.PLAYERS_TOOLTIP, true);
        standingsButton = initChildButton(screenSwitchPane, CSB_PropertyType.STANDINGS_ICON, CSB_PropertyType.STANDINGS_TOOLTIP, true);
        draftButton = initChildButton(screenSwitchPane, CSB_PropertyType.DRAFT_ICON, CSB_PropertyType.DRAFT_TOOLTIP, true);
        MLBButton = initChildButton(screenSwitchPane, CSB_PropertyType.MLB_TEAMS_ICON, CSB_PropertyType.MLB_TEAMS_TOOLTIP, true);
    }
    
    public void reloadPlayersList(int i){
        switch(i){
            case 0: visiblePlayers = FXCollections.observableArrayList();
                for(int j = 0; j < players.size(); j++){
                    Player pl = players.get(j);
                    if(TextFieldContent.equals("") || (pl.getFirstName().toLowerCase().startsWith(TextFieldContent.toLowerCase())) || 
                        (pl.getLastName().toLowerCase().startsWith(TextFieldContent.toLowerCase()))){
                        visiblePlayers.add(pl);
                    }
                }
                playersTable.setItems(visiblePlayers);
                break;
            case 1: visiblePlayers = FXCollections.observableArrayList();
                for(int j = 0; j < players.size(); j++){
                    Player pl = players.get(j);
                    if(TextFieldContent.equals("") || (pl.getFirstName().toLowerCase().startsWith(TextFieldContent.toLowerCase())) || 
                        (pl.getLastName().toLowerCase().startsWith(TextFieldContent.toLowerCase()))){
                        if(pl.getPositions().contains("C")){
                        visiblePlayers.add(pl);
                        }
                    }
                }
                playersTable.setItems(visiblePlayers);
                break;
            case 2:visiblePlayers = FXCollections.observableArrayList();
                for(int j = 0; j < players.size(); j++){
                    Player pl = players.get(j);
                    if(TextFieldContent.equals("") || (pl.getFirstName().toLowerCase().startsWith(TextFieldContent.toLowerCase())) || 
                        (pl.getLastName().toLowerCase().startsWith(TextFieldContent.toLowerCase()))){
                        if(pl.getPositions().contains("1B")){
                        visiblePlayers.add(pl);
                        }
                    }
                }
                playersTable.setItems(visiblePlayers);
                break;
            case 3:visiblePlayers = FXCollections.observableArrayList();
                for(int j = 0; j < players.size(); j++){
                    Player pl = players.get(j);
                    if(TextFieldContent.equals("") || (pl.getFirstName().toLowerCase().startsWith(TextFieldContent.toLowerCase())) || 
                        (pl.getLastName().toLowerCase().startsWith(TextFieldContent.toLowerCase()))){
                        if(pl.getPositions().contains("1B") || pl.getPositions().contains("3B")){
                        visiblePlayers.add(pl);
                        }
                    }
                }
                playersTable.setItems(visiblePlayers);
                break;
            case 4:visiblePlayers = FXCollections.observableArrayList();
                for(int j = 0; j < players.size(); j++){
                    Player pl = players.get(j);
                    if(TextFieldContent.equals("") || (pl.getFirstName().toLowerCase().startsWith(TextFieldContent.toLowerCase())) || 
                        (pl.getLastName().toLowerCase().startsWith(TextFieldContent.toLowerCase()))){
                        if(pl.getPositions().contains("3B")){
                        visiblePlayers.add(pl);
                        }
                    }
                }
                playersTable.setItems(visiblePlayers);
                break;
            case 5:visiblePlayers = FXCollections.observableArrayList();
                for(int j = 0; j < players.size(); j++){
                    Player pl = players.get(j);
                    if(TextFieldContent.equals("") || (pl.getFirstName().toLowerCase().startsWith(TextFieldContent.toLowerCase())) || 
                        (pl.getLastName().toLowerCase().startsWith(TextFieldContent.toLowerCase()))){
                        if(pl.getPositions().contains("2B")){
                        visiblePlayers.add(pl);
                        }
                    }
                }
                playersTable.setItems(visiblePlayers);
                break;
            case 6:visiblePlayers = FXCollections.observableArrayList();
                for(int j = 0; j < players.size(); j++){
                    Player pl = players.get(j);
                    if(TextFieldContent.equals("") || (pl.getFirstName().toLowerCase().startsWith(TextFieldContent.toLowerCase())) || 
                        (pl.getLastName().toLowerCase().startsWith(TextFieldContent.toLowerCase()))){
                        if(pl.getPositions().contains("2B") || pl.getPositions().contains("SS")){
                        visiblePlayers.add(pl);
                        }
                    }
                }
                playersTable.setItems(visiblePlayers);
                break;
            case 7:visiblePlayers = FXCollections.observableArrayList();
                for(int j = 0; j < players.size(); j++){
                    Player pl = players.get(j);
                    if(TextFieldContent.equals("") || (pl.getFirstName().toLowerCase().startsWith(TextFieldContent.toLowerCase())) || 
                        (pl.getLastName().toLowerCase().startsWith(TextFieldContent.toLowerCase()))){
                        if(pl.getPositions().contains("SS")){
                        visiblePlayers.add(pl);
                        }
                    }
                }
                playersTable.setItems(visiblePlayers);
                break;
            case 8:visiblePlayers = FXCollections.observableArrayList();
                for(int j = 0; j < players.size(); j++){
                    Player pl = players.get(j);
                    if(TextFieldContent.equals("") || (pl.getFirstName().toLowerCase().startsWith(TextFieldContent.toLowerCase())) || 
                        (pl.getLastName().toLowerCase().startsWith(TextFieldContent.toLowerCase()))){
                        if(pl.getPositions().contains("OF")){
                        visiblePlayers.add(pl);
                        }
                    }
                }
                playersTable.setItems(visiblePlayers);
                break;
            case 9:visiblePlayers = FXCollections.observableArrayList();
                for(int j = 0; j < players.size(); j++){
                    Player pl = players.get(j);
                    if(TextFieldContent.equals("") || (pl.getFirstName().toLowerCase().startsWith(TextFieldContent.toLowerCase())) || 
                        (pl.getLastName().toLowerCase().startsWith(TextFieldContent.toLowerCase()))){
                        if(!pl.getPositions().contains("P")){
                        visiblePlayers.add(pl);
                        }
                    }
                }
                playersTable.setItems(visiblePlayers);
                break;
            case 10:visiblePlayers = FXCollections.observableArrayList();
                for(int j = 0; j < players.size(); j++){
                    Player pl = players.get(j);
                    if(TextFieldContent.equals("") || (pl.getFirstName().toLowerCase().startsWith(TextFieldContent.toLowerCase())) || 
                        (pl.getLastName().toLowerCase().startsWith(TextFieldContent.toLowerCase()))){
                        if(pl.getPositions().contains("P")){
                        visiblePlayers.add(pl);
                        }
                    }
                }
                playersTable.setItems(visiblePlayers);
                break;
                
        }
    }
    
    /**
     * When called this function puts the players screen into the window,
     * for viewing and editing players
     */
    public void initPlayersScreen() throws IOException {
        playersPane = new VBox();
        playersPane.getStyleClass().add(CLASS_BORDERED_PANE);
        initRadioButtons();
        PlayersHeadingLabel = initChildLabel(playersPane, CSB_PropertyType.PLAYER_HEADING_LABEL, CLASS_HEADING_LABEL);
        searchField = new TextField();
        searchField.setEditable(true);
        searchField.setMinWidth(1000);
        playersToolbar = new HBox();
        addPlayerButton = initChildButton(playersToolbar, CSB_PropertyType.ADD_ICON, CSB_PropertyType.ADD_PLAYER_TOOLTIP, false);
        removePlayerButton = initChildButton(playersToolbar, CSB_PropertyType.MINUS_ICON, CSB_PropertyType.REMOVE_PLAYER_TOOLTIP, false);
        searchLabel = initChildLabel(playersToolbar, CSB_PropertyType.SEARCH_LABEL, CLASS_SUBHEADING_LABEL);
        searchLabel.setPadding(new Insets(0, 10, 0, 20));
        playersToolbar.getChildren().add(searchField);
        playersPane.getChildren().add(playersToolbar);
        playersPane.getChildren().add(hbRadio);
        initPlayersTable();
        playersScrollPane = new ScrollPane();
        playersScrollPane.setContent(playersPane);
        playersScrollPane.setFitToWidth(true);
        playersScrollPane.setFitToHeight(true);
        playersActivated = false;
    }
    
    void initPlayersTable() throws IOException{
        playersTable = new TableView();
        playersTable.setEditable(true);
        // NOW SETUP THE TABLE COLUMNS
        firstNameColumn = new TableColumn(COL_FIRST);
        lastNameColumn = new TableColumn(COL_LAST);
        teamColumn = new TableColumn(COL_TEAM);
        positionColumn = new TableColumn(COL_POSITIONS);
        birthColumn = new TableColumn(COL_BIRTH);
        RWColumn = new TableColumn(COL_RW);
        HRSVColumn = new TableColumn(COL_HRSV);
        RBIKColumn = new TableColumn(COL_RBIK);
        SBERAColumn = new TableColumn(COL_SBERA);
        BAWHIPColumn = new TableColumn(COL_BAWHIP);
        valueColumn = new TableColumn(COL_VALUE);
        notesColumn = new TableColumn(COL_NOTES);
        valueColumn.setMinWidth(100);
        players = getPlayers();
        
        // AND LINK THE COLUMNS TO THE DATA
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        teamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("proTeam"));
        positionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("positions"));
        birthColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("birth"));
        RWColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("RW"));
        HRSVColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("HRSV"));
        RBIKColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("RBIK"));
        SBERAColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("SBERA"));
        BAWHIPColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("BAWHIP"));
        valueColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("BAWHIPS"));
        notesColumn.setCellValueFactory(new PropertyValueFactory<String, String>("notes"));
        notesColumn.setCellFactory(TextFieldTableCell.<Player>forTableColumn());
        notesColumn.setEditable(true);
        playersTable.getColumns().add(firstNameColumn);
        playersTable.getColumns().add(lastNameColumn);
        playersTable.getColumns().add(teamColumn);
        playersTable.getColumns().add(positionColumn);
        playersTable.getColumns().add(birthColumn);
        playersTable.getColumns().add(RWColumn);
        playersTable.getColumns().add(HRSVColumn);
        playersTable.getColumns().add(RBIKColumn);
        playersTable.getColumns().add(SBERAColumn);
        playersTable.getColumns().add(BAWHIPColumn);
        playersTable.getColumns().add(valueColumn);
        playersTable.getColumns().add(notesColumn);
        playersPane.getChildren().add(playersTable);
        visiblePlayers = players;
        playersTable.setItems(visiblePlayers);
        
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }   
    
    public ObservableList<Player> getPlayerList(){
        return players;
    }
    
    public ObservableList<Player> getViewablePlayerList(){
        return visiblePlayers;
    }
    
    public ObservableList<Player> getDraftOrder(){
        return draftOrder;
    }
    
    public void setDraftOrder(ObservableList<Player> init){
        draftOrder = init;
    }
    
    ObservableList<Player> getPlayers() throws IOException{
        ObservableList<Player> playerList = FXCollections.observableArrayList();
        JsonObject jsonHit = loadJSONFile(HITTERS_PATH);
        JsonObject jsonPitch = loadJSONFile(PITCHERS_PATH);
        JsonArray jsonHittersArray = jsonHit.getJsonArray(JSON_HITTERS);
        for (int i = 0; i < jsonHittersArray.size(); i++) {
            JsonObject jso = jsonHittersArray.getJsonObject(i);
            Player pl = new Player();
            pl.setFirstName(jso.getString(JSON_PLAYER_FIRST));
            pl.setLastName(jso.getString(JSON_PLAYER_LAST));
            pl.setProTeam(jso.getString(JSON_TEAM));
            pl.setPositions(jso.getString(JSON_POSITION));
            pl.setBirth(Integer.parseInt(jso.getString(JSON_BIRTH)));
            pl.setRW(Integer.parseInt(jso.getString(JSON_RUNS)));
            pl.setHRSV(Integer.parseInt(jso.getString(JSON_HOMERUNS)));
            pl.setRBIK(Integer.parseInt(jso.getString(JSON_RBI)));
            pl.setSBERA(Double.parseDouble(jso.getString(JSON_STOLEN_BASE)));
            pl.setCountry(jso.getString(JSON_COUNTRY));
            if(Integer.parseInt(jso.getString(JSON_ATBAT)) == 0){
                pl.setBAWHIP(0);
            }
            else pl.setBAWHIP(round((Double.parseDouble(jso.getString(JSON_HITS))) / (Double.parseDouble(jso.getString(JSON_ATBAT))), 3));
            pl.setNotes(jso.getString(JSON_NOTES));
            playerList.add(pl);
        }
        JsonArray jsonPitchersArray = jsonPitch.getJsonArray(JSON_PITCHERS);
        for (int i = 0; i < jsonPitchersArray.size(); i++) {
            JsonObject jso = jsonPitchersArray.getJsonObject(i);
            Player pl = new Player();
            pl.setFirstName(jso.getString(JSON_PLAYER_FIRST));
            pl.setLastName(jso.getString(JSON_PLAYER_LAST));
            pl.setProTeam(jso.getString(JSON_TEAM));
            pl.setPositions("P");
            pl.setBirth(Integer.parseInt(jso.getString(JSON_BIRTH)));
            pl.setRW(Integer.parseInt(jso.getString(JSON_WINS)));
            pl.setHRSV(Integer.parseInt(jso.getString(JSON_SAVES)));
            pl.setRBIK(Integer.parseInt(jso.getString(JSON_STRIKES)));
            if(Double.parseDouble(jso.getString(JSON_INNINGS)) == 0){
                pl.setSBERA(0);
                pl.setRBIK(0);
            }
            else {
                pl.setSBERA(round(9*(Double.parseDouble(jso.getString(JSON_ER)) / Double.parseDouble(jso.getString(JSON_INNINGS))), 3));
                pl.setBAWHIP(round((Double.parseDouble(jso.getString(JSON_HITS)) + Double.parseDouble(jso.getString(JSON_BB))) / (Double.parseDouble(jso.getString(JSON_INNINGS))), 3));
            }
            pl.setNotes(jso.getString(JSON_NOTES));
            playerList.add(pl);
        }
        return playerList;
    }
    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
}
    
    void initRadioButtons(){
        hbRadio = new HBox();
        allButton = new RadioButton("All");
        CButton = new RadioButton("C");
        B1Button = new RadioButton("1B");
        CIButton = new RadioButton("CI");
        B3Button = new RadioButton("3B");
        B2Button = new RadioButton("2B");
        MIButton = new RadioButton("MI");
        SSButton = new RadioButton("SS");
        OFButton = new RadioButton("OF");
        UButton = new RadioButton("U");
        PButton = new RadioButton("P");
        group = new ToggleGroup();
        allButton.setToggleGroup(group);
        CButton.setToggleGroup(group);
        B1Button.setToggleGroup(group);
        CIButton.setToggleGroup(group);
        B3Button.setToggleGroup(group);
        B2Button.setToggleGroup(group);
        MIButton.setToggleGroup(group);
        SSButton.setToggleGroup(group);
        OFButton.setToggleGroup(group);
        UButton.setToggleGroup(group);
        PButton.setToggleGroup(group);
        allButton.setPadding(new Insets(5, 30, 5, 30));
        CButton.setPadding(new Insets(5, 30, 5, 30));
        B1Button.setPadding(new Insets(5, 30, 5, 30));
        CIButton.setPadding(new Insets(5, 30, 5, 30));
        B3Button.setPadding(new Insets(5, 30, 5, 30));
        B2Button.setPadding(new Insets(5, 30, 5, 30));
        MIButton.setPadding(new Insets(5, 30, 5, 30));
        SSButton.setPadding(new Insets(5, 30, 5, 30));
        OFButton.setPadding(new Insets(5, 30, 5, 30));
        UButton.setPadding(new Insets(5, 30, 5, 30));
        PButton.setPadding(new Insets(5, 30, 5, 30));
        hbRadio.getChildren().add(allButton);
        hbRadio.getChildren().add(CButton);
        hbRadio.getChildren().add(B1Button);
        hbRadio.getChildren().add(CIButton);
        hbRadio.getChildren().add(B3Button);
        hbRadio.getChildren().add(B2Button);
        hbRadio.getChildren().add(MIButton);
        hbRadio.getChildren().add(SSButton);
        hbRadio.getChildren().add(OFButton);
        hbRadio.getChildren().add(UButton);
        hbRadio.getChildren().add(PButton);
        allButton.fire();
    }
    
    /**
     * When called this function puts the players screen into the window,
     * for viewing and editing players
     */
    public void initStandingsScreen() {
        standingsPane = new VBox();
        standingsPane.getStyleClass().add(CLASS_BORDERED_PANE);
        StandingsHeadingLabel = initChildLabel(standingsPane, CSB_PropertyType.STANDINGS_HEADING_LABEL, CLASS_HEADING_LABEL);
        standingsScrollPane = new ScrollPane();
        fantasyTeamsTable = new TableView<Team>();
        teamNameColumn = new TableColumn(CteamNameColumn);
        teamPlayersNeededColumn = new TableColumn(CteamPlayersNeededColumn);
        teamDollarsColumn = new TableColumn(CteamDollarsColumn);
        teamPerPlayerColumn = new TableColumn(CteamPerPlayerColumn);
        teamRColumn = new TableColumn(CteamRColumn);
        teamHRColumn = new TableColumn(CteamHRColumn);
        teamRBIColumn = new TableColumn(CteamRBIColumn);
        teamSBColumn = new TableColumn(CteamSBColumn);
        teamBAColumn = new TableColumn(CteamBAColumn);
        teamWColumn = new TableColumn(CteamWColumn);
        teamSVColumn = new TableColumn(CteamSVColumn);
        teamKColumn = new TableColumn(CteamKColumn);
        teamERAColumn = new TableColumn(CteamNameERAColumn);
        teamWHIPColumn = new TableColumn(CteamWHIPColumn);
        teamPointsColumn = new TableColumn(CteamPointsColumn);
        
        teamNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        teamPlayersNeededColumn.setCellValueFactory(new PropertyValueFactory<String, String>("playersNeeded"));
        teamDollarsColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("dollars"));
        teamPerPlayerColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("perPlayer"));
        teamRColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("R"));
        teamHRColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("HR"));
        teamRBIColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("RBI"));
        teamSBColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("SB"));
        teamBAColumn.setCellValueFactory(new PropertyValueFactory<String, Double>("BA"));
        teamWColumn.setCellValueFactory(new PropertyValueFactory<String, Double>("W"));
        teamSVColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("SV"));
        teamKColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("K"));
        teamERAColumn.setCellValueFactory(new PropertyValueFactory<String, Double>("ERA"));
        teamWHIPColumn.setCellValueFactory(new PropertyValueFactory<String, Double>("WHIP"));
        teamPointsColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("totalPoints"));
        fantasyTeamsTable.getColumns().add(teamNameColumn);
        fantasyTeamsTable.getColumns().add(teamPlayersNeededColumn);
        fantasyTeamsTable.getColumns().add(teamDollarsColumn);
        fantasyTeamsTable.getColumns().add(teamPerPlayerColumn);
        fantasyTeamsTable.getColumns().add(teamRColumn);
        fantasyTeamsTable.getColumns().add(teamHRColumn);
        fantasyTeamsTable.getColumns().add(teamRBIColumn);
        fantasyTeamsTable.getColumns().add(teamSBColumn);
        fantasyTeamsTable.getColumns().add(teamBAColumn);
        fantasyTeamsTable.getColumns().add(teamWColumn);
        fantasyTeamsTable.getColumns().add(teamSVColumn);
        fantasyTeamsTable.getColumns().add(teamKColumn);
        fantasyTeamsTable.getColumns().add(teamERAColumn);
        fantasyTeamsTable.getColumns().add(teamWHIPColumn);
        fantasyTeamsTable.getColumns().add(teamPointsColumn);
        fantasyTeamsTable.setItems(draft.getTeams());
        standingsPane.getChildren().add(fantasyTeamsTable);
        standingsScrollPane.setContent(standingsPane);
        standingsScrollPane.setFitToWidth(true);
        standingsScrollPane.setFitToHeight(true);
        standingsActivated = false;
    }
    
    public void reloadFantasyTeams(){
        fantasyTeamsTable.setItems(null);
        fantasyTeamsTable.setItems(draft.getTeams());
    }
    
    /**
     * When called this function puts the teams screen into the window,
     * for viewing and editing players and teams
     */
    public void initFantasyTeams() {
        teamsPane = new VBox();
        teamsPane.getStyleClass().add(CLASS_BORDERED_PANE);
        TeamsHeadingLabel = initChildLabel(teamsPane, CSB_PropertyType.TEAMS_HEADING_LABEL, CLASS_HEADING_LABEL);
        teamsScrollPane = new ScrollPane();
        teamsScrollPane.setContent(teamsPane);
        teamsScrollPane.setFitToWidth(true);
        teamsScrollPane.setFitToHeight(true);
        initTeamOps();
        initTables();
        teamsPane.getChildren().add(teamOpsPane);
        teamsPane.getChildren().add(startingLineupBox);
        teamsPane.getChildren().add(taxiSquadBox);
        teamsActivated = false;
    }
    
    private void initTeamOps(){
        teamOpsPane = new GridPane();
        dNameLabel = new Label("Draft Name: ");
        dNameTextField = new TextField();
        dNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            draft.setName(newValue);
        });
        teamSelectLabel = new Label("Select Fantasy Team");
        teamSelectComboBox = new ComboBox();
        updateTeamSelect();
        addTeamButton = new Button();
        removeTeamButton = new Button();
        editTeamButton = new Button();
        String imagePath = "file:" + PATH_IMAGES + "Add.png";
        String image2Path = "file:" + PATH_IMAGES + "Minus.png";
        String image3Path = "file:" + PATH_IMAGES + "Edit.png";
        Image buttonImage = new Image(imagePath);
        Image button2Image = new Image(image2Path);
        Image button3Image = new Image(image3Path);
        addTeamButton.setDisable(false);
        removeTeamButton.setDisable(false);
        editTeamButton.setDisable(false);
        addTeamButton.setGraphic(new ImageView(buttonImage));
        removeTeamButton.setGraphic(new ImageView(button2Image));
        editTeamButton.setGraphic(new ImageView(button3Image));
        Tooltip buttonTooltip = new Tooltip("Add a Team");
        Tooltip button2Tooltip = new Tooltip("Remove selected team");
        Tooltip button3Tooltip = new Tooltip("Edit selected team");
        addTeamButton.setTooltip(buttonTooltip);
        removeTeamButton.setTooltip(button2Tooltip);
        editTeamButton.setTooltip(button3Tooltip);
        teamOpsPane.add(dNameLabel, 0, 0, 2, 1);
        teamOpsPane.add(dNameTextField, 2, 0, 2, 1);
        teamOpsPane.add(addTeamButton, 0, 1, 1, 1);
        teamOpsPane.add(removeTeamButton, 1, 1, 1, 1);
        teamOpsPane.add(editTeamButton, 2, 1, 1, 1);
        teamOpsPane.add(teamSelectLabel, 4, 1, 1, 1);
        teamOpsPane.add(teamSelectComboBox, 5, 1, 2, 1);
        teamSelectComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            for(Team te: dataManager.getDraft().getTeams()){
                if(te.getName().equalsIgnoreCase(newValue.toString())){
                    updateLineup(te);
                    break;
                }
            }
        });
        
    }
    private void updateTeamSelect(){
        ObservableList<String> teams = FXCollections.observableArrayList();
        for(int i = 0; i < draft.getTeams().size(); i++){
            teams.add(draft.getTeams().get(i).getName());
        }
        teamSelectComboBox.setItems(teams);
    }
    
    public Team getTeamSelected(){
        for(Team te: draft.getTeams()){
            if(teamSelectComboBox.getValue() != null && te.getName().equalsIgnoreCase(teamSelectComboBox.getValue().toString())){
                return te;
            }
        }
        return null;
    }
    
    private void initTables(){
        startingLineupBox = new VBox();
        startingLineupBox.setPadding(new Insets(10, 10, 10, 10));
        startingLineupLabel = initChildLabel(startingLineupBox, CSB_PropertyType.LINEUP_LABEL, CLASS_HEADING_LABEL);
        taxiSquadBox = new VBox();
        taxiSquadBox.setPadding(new Insets(10, 10, 10, 10));
        taxiSquadLabel = initChildLabel(taxiSquadBox, CSB_PropertyType.TAXI_LABEL, CLASS_HEADING_LABEL);
        startingLineup = new TableView();
        startingLineup.setEditable(false);
        // NOW SETUP THE TABLE COLUMNS
        sActualPositionColumn = new TableColumn(COL_POSITION);
        sFirstNameColumn = new TableColumn(COL_FIRST);
        sLastNameColumn = new TableColumn(COL_LAST);
        sTeamColumn = new TableColumn(COL_TEAM);
        sPositionColumn = new TableColumn(COL_POSITIONS);
        sRWColumn = new TableColumn(COL_RW);
        sHRSVColumn = new TableColumn(COL_HRSV);
        sRBIKColumn = new TableColumn(COL_RBIK);
        sSBERAColumn = new TableColumn(COL_SBERA);
        sBAWHIPColumn = new TableColumn(COL_BAWHIP);
        sValueColumn = new TableColumn(COL_VALUE);
        sContractColumn = new TableColumn(COL_CONTRACT);
        sSalaryColumn = new TableColumn(COL_SALARY);
        
        // AND LINK THE COLUMNS TO THE DATA
        sActualPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        sFirstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        sLastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        sTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("proTeam"));
        sPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("positions"));
        sRWColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("RW"));
        sHRSVColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("HRSV"));
        sRBIKColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("RBIK"));
        sSBERAColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("SBERA"));
        sBAWHIPColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("BAWHIP"));
        sValueColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("BAWHIPS"));
        sContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        sSalaryColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("salary"));
        startingLineup.getColumns().add(sActualPositionColumn);
        startingLineup.getColumns().add(sFirstNameColumn);
        startingLineup.getColumns().add(sLastNameColumn);
        startingLineup.getColumns().add(sTeamColumn);
        startingLineup.getColumns().add(sPositionColumn);
        startingLineup.getColumns().add(sRWColumn);
        startingLineup.getColumns().add(sHRSVColumn);
        startingLineup.getColumns().add(sRBIKColumn);
        startingLineup.getColumns().add(sSBERAColumn);
        startingLineup.getColumns().add(sBAWHIPColumn);
        startingLineup.getColumns().add(sValueColumn);
        startingLineup.getColumns().add(sContractColumn);
        startingLineup.getColumns().add(sSalaryColumn);
        startingLineup.sortPolicyProperty().set(new Callback<TableView<Player>, Boolean>() {
        @Override
        public Boolean call(TableView<Player> param) {
            Comparator<Player> comparator = new Comparator<Player>() {

                @Override
                public int compare(Player p1, Player p2) {
                    if (convertPositionToInteger(p1.getPosition()) > convertPositionToInteger(p2.getPosition())) {
                    return 1;
                } else if (convertPositionToInteger(p1.getPosition()) < convertPositionToInteger(p2.getPosition())) {
                    return -1;
                } else if (convertPositionToInteger(p1.getPosition()) == convertPositionToInteger(p2.getPosition())) {
                    return 0;
                } else {
                    return param.getComparator().compare(p1, p2);
                }
                }
            };
            FXCollections.sort(startingLineup.getItems(), comparator);
            return true;  
        }
        });
        
        taxiSquad = new TableView();
        taxiSquad.setEditable(false);
        // NOW SETUP THE TABLE COLUMNS
        tFirstNameColumn = new TableColumn(COL_FIRST);
        tLastNameColumn = new TableColumn(COL_LAST);
        tTeamColumn = new TableColumn(COL_TEAM);
        tPositionColumn = new TableColumn(COL_POSITIONS);
        tRWColumn = new TableColumn(COL_RW);
        tHRSVColumn = new TableColumn(COL_HRSV);
        tRBIKColumn = new TableColumn(COL_RBIK);
        tSBERAColumn = new TableColumn(COL_SBERA);
        tBAWHIPColumn = new TableColumn(COL_BAWHIP);
        tValueColumn = new TableColumn(COL_VALUE);
        tContractColumn = new TableColumn(COL_CONTRACT);
        tSalaryColumn = new TableColumn(COL_SALARY);
        tActualPositionColumn = new TableColumn(COL_POSITION);
        
        // AND LINK THE COLUMNS TO THE DATA
        tFirstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        tLastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        tTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("proTeam"));
        tPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("positions"));
        tRWColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("RW"));
        tHRSVColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("HRSV"));
        tRBIKColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("RBIK"));
        tSBERAColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("SBERA"));
        tBAWHIPColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("BAWHIP"));
        tValueColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("BAWHIPS"));
        tContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        tSalaryColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("salary"));
        tActualPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("actualPosition"));
        taxiSquad.getColumns().add(tFirstNameColumn);
        taxiSquad.getColumns().add(tLastNameColumn);
        taxiSquad.getColumns().add(tTeamColumn);
        taxiSquad.getColumns().add(tPositionColumn);
        taxiSquad.getColumns().add(tRWColumn);
        taxiSquad.getColumns().add(tHRSVColumn);
        taxiSquad.getColumns().add(tRBIKColumn);
        taxiSquad.getColumns().add(tSBERAColumn);
        taxiSquad.getColumns().add(tBAWHIPColumn);
        taxiSquad.getColumns().add(tValueColumn);
        startingLineupBox.getChildren().add(startingLineup);
        taxiSquadBox.getChildren().add(taxiSquad);
    }
    
    public Integer convertPositionToInteger(String str){
        switch(str){
            case "C": return 1;
            case "1B": return 2;
            case "CI": return 3;
            case "3B": return 4;
            case "2B": return 5;
            case "MI": return 6;
            case "SS": return 7;
            case "OF": return 8;
            case "U": return 9;
            case "P": return 10; 
        }
        return null;
    }
    
    public void updateLineup(Team teamToUse){
        startingLineup.setItems(teamToUse.getPlayers());
        taxiSquad.setItems(teamToUse.getTaxi());
        startingLineup.sort();
    }
    
    /**
     * When called this function puts the players screen into the window,
     * for viewing and editing players
     */
    public void initMLBTeamScreen() {
        MLBPane = new VBox();
        MLBPane.getStyleClass().add(CLASS_BORDERED_PANE);
        MLBHeadingLabel = initChildLabel(MLBPane, CSB_PropertyType.MLB_HEADING_LABEL, CLASS_HEADING_LABEL);
        proTeamLabel = initChildLabel(MLBPane, CSB_PropertyType.MLB_TEAMS_LABEL, CLASS_PROMPT_LABEL);
        proTeamComboBox = new ComboBox();
        ObservableList<String> proTeams = FXCollections.observableArrayList();
        proTeams.add("ATL");
        proTeams.add("AZ");
        proTeams.add("CHC");
        proTeams.add("CIN");
        proTeams.add("COL");
        proTeams.add("LAD");
        proTeams.add("MIA");
        proTeams.add("MIL");
        proTeams.add("NYM");
        proTeams.add("PHI");
        proTeams.add("PIT");
        proTeams.add("SD");
        proTeams.add("SF");
        proTeams.add("STL");
        proTeams.add("WSH");
        proTeamComboBox.setItems(proTeams);
        MLBPane.getChildren().add(proTeamComboBox);
        MLBTeamsTable = new TableView<Player>();
        MLBTeamsTable.setEditable(false);
        MLBTeamList = FXCollections.observableArrayList();
        MLBFirst = new TableColumn(COL_FIRST);
        MLBLast = new TableColumn(COL_LAST);
        MLBPositions = new TableColumn(COL_POSITIONS);
        MLBFirst.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        MLBLast.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        MLBPositions.setCellValueFactory(new PropertyValueFactory<String, String>("positions"));
        MLBTeamsTable.getColumns().add(MLBFirst);
        MLBTeamsTable.getColumns().add(MLBLast);
        MLBTeamsTable.getColumns().add(MLBPositions);
        MLBTeamsTable.setItems(MLBTeamList);
        MLBPane.getChildren().add(MLBTeamsTable);
        proTeamComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            updateProTeams(proTeamComboBox.getValue().toString());
        });
        MLBScrollPane = new ScrollPane();
        MLBScrollPane.setContent(MLBPane);
        MLBScrollPane.setFitToWidth(true);
        MLBScrollPane.setFitToHeight(true);
        MLBActivated = false;
    }
    
    public void updateProTeams(String teamString){
        MLBTeamList.clear();
        for(Team te: dataManager.getDraft().getTeams()){
            for(Player pl: te.getPlayers()){
                if(pl.getProTeam().equalsIgnoreCase(teamString)){
                    MLBTeamList.add(pl);
                }
            }
        }
        for(Player pl: players){
            if(pl.getProTeam().equalsIgnoreCase(teamString)){
                    MLBTeamList.add(pl);
                }
        }
        MLBTeamsTable.setItems(MLBTeamList);
    }
    
    /**
     * When called this function puts the players screen into the window,
     * for viewing and editing players
     */
    public void initDraftScreen() {
        draftPane = new VBox();
        draftPane.getStyleClass().add(CLASS_BORDERED_PANE);
        DraftHeadingLabel = initChildLabel(draftPane, CSB_PropertyType.DRAFT_HEADING_LABEL, CLASS_HEADING_LABEL);
        draftOpsPane = new GridPane();
        pickButton = new Button();
        autoPickButton = new Button();
        pauseButton = new Button();
        String imagePath = "file:" + PATH_IMAGES + "Pick.png";
        String image2Path = "file:" + PATH_IMAGES + "Autopick.png";
        String image3Path = "file:" + PATH_IMAGES + "Pause.png";
        Image buttonImage = new Image(imagePath);
        Image button2Image = new Image(image2Path);
        Image button3Image = new Image(image3Path);
        pickButton.setDisable(false);
        autoPickButton.setDisable(false);
        pauseButton.setDisable(false);
        pickButton.setGraphic(new ImageView(buttonImage));
        autoPickButton.setGraphic(new ImageView(button2Image));
        pauseButton.setGraphic(new ImageView(button3Image));
        Tooltip buttonTooltip = new Tooltip("Draft a Player");
        Tooltip button2Tooltip = new Tooltip("Automatic Draft");
        Tooltip button3Tooltip = new Tooltip("Pause Automatic Draft");
        pickButton.setTooltip(buttonTooltip);
        autoPickButton.setTooltip(button2Tooltip);
        pauseButton.setTooltip(button3Tooltip);
        draftOpsPane.add(pickButton, 0, 0, 1, 1);
        draftOpsPane.add(autoPickButton, 1, 0, 1, 1);
        draftOpsPane.add(pauseButton, 2, 0, 1, 1);
        draftPane.getChildren().add(draftOpsPane);
        draftOrder = FXCollections.observableArrayList();
        draftList = new TableView<>();
        pickColumn = new TableColumn("Pick #");
        draftFNameColumn = new TableColumn(COL_FIRST);
        draftLNameColumn = new TableColumn(COL_LAST);
        draftTeamNameColumn = new TableColumn("Team Name");
        draftContractColumn = new TableColumn(COL_CONTRACT);
        draftSalaryColumn = new TableColumn(COL_SALARY);
        pickColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("transactionNumber"));
        draftFNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        draftLNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        draftTeamNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("draftTeam"));
        draftContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        draftSalaryColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("salary"));
        draftList.getColumns().add(pickColumn);
        draftList.getColumns().add(draftFNameColumn);
        draftList.getColumns().add(draftLNameColumn);
        draftList.getColumns().add(draftTeamNameColumn);
        draftList.getColumns().add(draftContractColumn);
        draftList.getColumns().add(draftSalaryColumn);
        draftScrollPane = new ScrollPane();
        draftList.setItems(draftOrder);
        draftList.sortPolicyProperty().set(new Callback<TableView<Player>, Boolean>() {
        @Override
        public Boolean call(TableView<Player> param) {
            Comparator<Player> comparator = new Comparator<Player>() {

                @Override
                public int compare(Player p1, Player p2) {
                    if (p1.getTransactionNumber() > p2.getTransactionNumber()) {
                    return 1;
                } else if (p1.getTransactionNumber() < p2.getTransactionNumber()) {
                    return -1;
                } else if (p1.getTransactionNumber() == p2.getTransactionNumber()) {
                    return 0;
                } else {
                    return param.getComparator().compare(p1, p2);
                }
                }
            };
            FXCollections.sort(draftOrder, comparator);
            FXCollections.sort(draftList.getItems(), comparator);
            return true;  
        }
        });
        draftPane.getChildren().add(draftList);
        draftScrollPane.setContent(draftPane);
        draftScrollPane.setFitToWidth(true);
        draftScrollPane.setFitToHeight(true);
        draftActivated = false;
    }
    
    public void addDraftPlayer(Player pl){
        draftOrder.add(pl);
    }
    
    public void removeDraftPlayer(Player pl){
        Player removeMe = new Player();
        for(Player p: draftOrder){
            if(p.getFirstName().equalsIgnoreCase(pl.getFirstName()) && p.getLastName().equals(pl.getLastName())){
                removeMe = p;
            }
        }
        draftOrder.remove(removeMe);
        for(int i = 0; i < draftOrder.size(); i++){
            draftOrder.get(i).setTransactionNumber(i + 1);
        }
    }
    
    /**
     * When called this function puts the players screen into the window,
     * for viewing and editing players
     */
    public void activatePlayersScreen() {
        if (!playersActivated) {
            // PUT THE WORKSPACE IN THE GUI
            csbPane.setCenter(playersScrollPane);
            playersActivated = true;
            teamsActivated = false;
            draftActivated = false;
            standingsActivated = false;
            MLBActivated = false;
        }
    }
    /**
     * When called this function puts the players screen into the window,
     * for viewing and editing players
     */
    public void activateStandingsScreen() {
        if (!standingsActivated) {
            // PUT THE WORKSPACE IN THE GUI
            csbPane.setCenter(standingsScrollPane);
            playersActivated = false;
            teamsActivated = false;
            draftActivated = false;
            standingsActivated = true;
            MLBActivated = false;
        }
    }
    /**
     * When called this function puts the players screen into the window,
     * for viewing and editing players
     */
    public void activateFantasyTeams() {
        if (!teamsActivated) {
            // PUT THE WORKSPACE IN THE GUI
            csbPane.setCenter(teamsScrollPane);
            playersActivated = false;
            teamsActivated = true;
            draftActivated = false;
            standingsActivated = false;
            MLBActivated = false;
        }
    }
    /**
     * When called this function puts the players screen into the window,
     * for viewing and editing players
     */
    public void activateMLBTeamScreen() {
        if (!MLBActivated) {
            // PUT THE WORKSPACE IN THE GUI
            csbPane.setCenter(MLBScrollPane);
            playersActivated = false;
            teamsActivated = false;
            draftActivated = false;
            standingsActivated = false;
            MLBActivated = true;
        }
    }
    /**
     * When called this function puts the players screen into the window,
     * for viewing and editing players
     */
    public void activateDraftScreen() {
        if (!draftActivated) {
            // PUT THE WORKSPACE IN THE GUI
            csbPane.setCenter(draftScrollPane);
            playersActivated = false;
            teamsActivated = false;
            draftActivated = true;
            standingsActivated = false;
            MLBActivated = false;
        }
    }
    public String[] getActivePositions(){
        String str[];
        if(all){
            str = new String[]{"C", "1B", "3B", "2B", "SS", "OF", "P"};
        }
        else if(C){
            str = new String[]{"C"};
        }
        else if(B1){
            str = new String[]{"1B"};
        }
        else if(CI){
            str = new String[]{"1B", "3B"};
        }
        else if(B3){
            str = new String[]{"3B"};
        }
        else if(B2){
            str = new String[]{"2B"};
        }
        else if(MI){
            str = new String[]{"2B", "SS"};
        }
        else if(SS){
            str = new String[]{"SS"};
        }
        else if(OF){
            str = new String[]{"OF"};
        }
        else if(U){
            str = new String[]{"C", "1B", "3B", "2B", "SS", "OF"};
        }
        else if(P){
            str = new String[]{"P"};
        }
        else str = new String[]{""};
        return str;
    }
    
    
    
    /**
     * This function takes all of the data out of the courseToReload 
     * argument and loads its values into the user interface controls.
     * 
     * @param courseToReload The Course whose data we'll load into the GUI.
     */
    @Override
    public void reloadDraft(Draft draftToReload) {
        dNameTextField.setText(draftToReload.getName());
        updateTeamSelect();
        this.updateLineup(new Team());
        allButton.fire();
        reloadPlayersList(0);
        all = true;
        C = false;
        B1 = false;
        CI = false;
        B3 = false;
        B2 = false;
        MI = false;
        SS = false;
        OF = false;
        U = false;
        P = false;
    }

    /**
     * This method is used to activate/deactivate toolbar buttons when
     * they can and cannot be used so as to provide foolproof design.
     * 
     * @param saved Describes whether the loaded Course has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        saveCourseButton.setDisable(saved);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
        loadCourseButton.setDisable(false);
        exportSiteButton.setDisable(false);
        playersButton.setDisable(false);
        teamsButton.setDisable(false);
        draftButton.setDisable(false);
        standingsButton.setDisable(false);
        MLBButton.setDisable(false);

        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }

    

    /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /****************************************************************************/
    
    private void initDialogs() {
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
        progressDialog = new ProgressDialog(primaryStage, "Preparing");
    }
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newCourseButton = initChildButton(fileToolbarPane, CSB_PropertyType.NEW_COURSE_ICON, CSB_PropertyType.NEW_COURSE_TOOLTIP, false);
        loadCourseButton = initChildButton(fileToolbarPane, CSB_PropertyType.LOAD_COURSE_ICON, CSB_PropertyType.LOAD_COURSE_TOOLTIP, true);
        saveCourseButton = initChildButton(fileToolbarPane, CSB_PropertyType.SAVE_COURSE_ICON, CSB_PropertyType.SAVE_COURSE_TOOLTIP, true);
        exportSiteButton = initChildButton(fileToolbarPane, CSB_PropertyType.EXPORT_PAGE_ICON, CSB_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        exitButton = initChildButton(fileToolbarPane, CSB_PropertyType.EXIT_ICON, CSB_PropertyType.EXIT_TOOLTIP, false);
    }

    

    // INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS
    // THERE EXCEPT THE WORKSPACE, WHICH WILL BE ADDED THE FIRST
    // TIME A NEW Course IS CREATED OR LOADED
    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        csbPane = new BorderPane();
        csbPane.setTop(fileToolbarPane);
        csbPane.setBottom(screenSwitchPane);
        primaryScene = new Scene(csbPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    // INIT ALL THE EVENT HANDLERS
    private void initEventHandlers() throws IOException {
        // FIRST THE FILE CONTROLS
        fileController = new FileController(messageDialog, yesNoCancelDialog, progressDialog, courseFileManager, siteExporter);
        newCourseButton.setOnAction(e -> {
            try {
                players = getPlayers();
            } catch (IOException ex) {
                Logger.getLogger(CSB_GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            draftOrder.clear();
            draft.setTransactionNumber(1);
            fileController.handleNewDraftRequest(this);
        });
        loadCourseButton.setOnAction(e -> {
            fileController.handleLoadDraftRequest(this);
            teamSelectComboBox.setValue(null);
        });
        saveCourseButton.setOnAction(e -> {
            if(!dNameTextField.getText().equals("")){
                fileController.handleSaveDraftRequest(this, dataManager.getDraft());
            }
        });
        /*exportSiteButton.setOnAction(e -> {
            fileController.handleExportCourseRequest(this);
        });*/
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest(this);
        });
        playersButton.setOnAction(e -> {
            activatePlayersScreen();
        });
        teamsButton.setOnAction(e -> {
            activateFantasyTeams();
        });
        standingsButton.setOnAction(e -> {
            activateStandingsScreen();
        });
        draftButton.setOnAction(e -> {
            activateDraftScreen();
        });
        MLBButton.setOnAction(e -> {
            activateMLBTeamScreen();
        });
        registerSearchField(searchField);
        allButton.setOnAction(e->{
           reloadPlayersList(0);
           all = true;
           C = false;
           B1 = false;
           CI = false;
           B3 = false;
           B2 = false;
           MI = false;
           SS = false;
           OF = false;
           U = false;
           P = false;
           
        });
        CButton.setOnAction(e->{
            reloadPlayersList(1);
            all = false;
            C = true;
            B1 = false;
            CI = false;
            B3 = false;
            B2 = false;
            MI = false;
            SS = false;
            OF = false;
            U = false;
            P = false;
        });
        B1Button.setOnAction(e->{
            reloadPlayersList(2);
            all = false;
            C = false;
            B1 = true;
            CI = false;
            B3 = false;
            B2 = false;
            MI = false;
            SS = false;
            OF = false;
            U = false;
            P = false;
        });
        CIButton.setOnAction(e->{
            reloadPlayersList(3);
            all = false;
            C = false;
            B1 = false;
            CI = true;
            B3 = false;
            B2 = false;
            MI = false;
            SS = false;
            OF = false;
            U = false;
            P = false;
        });
        B3Button.setOnAction(e->{
            reloadPlayersList(4);
            all = false;
            C = false;
            B1 = false;
            CI = false;
            B3 = true;
            B2 = false;
            MI = false;
            SS = false;
            OF = false;
            U = false;
            P = false;
        });
        B2Button.setOnAction(e->{
            reloadPlayersList(5);
            all = false;
            C = false;
            B1 = false;
            CI = false;
            B3 = false;
            B2 = true;
            MI = false;
            SS = false;
            OF = false;
            U = false;
            P = false;
        });
        MIButton.setOnAction(e->{
            reloadPlayersList(6);
            all = false;
            C = false;
            B1 = false;
            CI = false;
            B3 = false;
            B2 = false;
            MI = true;
            SS = false;
            OF = false;
            U = false;
            P = false;
        });
        SSButton.setOnAction(e->{
            reloadPlayersList(7);
            all = false;
            C = false;
            B1 = false;
            CI = false;
            B3 = false;
            B2 = false;
            MI = false;
            SS = true;
            OF = false;
            U = false;
            P = false;
        });
        OFButton.setOnAction(e->{
            reloadPlayersList(8);
            all = false;
            C = false;
            B1 = false;
            CI = false;
            B3 = false;
            B2 = false;
            MI = false;
            SS = false;
            OF = true;
            U = false;
            P = false;
        });
        UButton.setOnAction(e->{
            reloadPlayersList(9);
            all = false;
            C = false;
            B1 = false;
            CI = false;
            B3 = false;
            B2 = false;
            MI = false;
            SS = false;
            OF = false;
            U = true;
            P = false;
        });
        PButton.setOnAction(e->{
            reloadPlayersList(10);
            all = false;
            C = false;
            B1 = false;
            CI = false;
            B3 = false;
            B2 = false;
            MI = false;
            SS = false;
            OF = false;
            U = false;
            P = true;
        });
        playerController = new PlayerEditController(primaryStage, playersTable.getSelectionModel().getSelectedItem(), draft,  messageDialog, yesNoCancelDialog);
        playersTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                // OPEN UP THE SCHEDULE ITEM EDITOR
                Player si = playersTable.getSelectionModel().getSelectedItem();
                playerController.handleEditPlayerRequest(this, si);
            }
        });
        taxiSquad.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                // OPEN UP THE SCHEDULE ITEM EDITOR
                Player si = (Player)taxiSquad.getSelectionModel().getSelectedItem();
                playerController.handleEditPlayerRequest(this, si);
            }
        });
        removePlayerButton.setOnAction(e->{
            Player si = playersTable.getSelectionModel().getSelectedItem();
            playerController.handleRemovePlayerRequest(this, si);
        });
        addPlayerButton.setOnAction(e->{
            Player si = playersTable.getSelectionModel().getSelectedItem();
            playerController.handleAddPlayerRequest(this);
        });
        teamController = new TeamEditController(primaryStage, draft,  messageDialog, yesNoCancelDialog);
        addTeamButton.setOnAction(e->{
            if(draft.getTeams().size() < 12){
                teamController.handleAddTeamRequest(this);
                updateTeamSelect();
            }
        });
        removeTeamButton.setOnAction(e->{
            String str = (String) teamSelectComboBox.getSelectionModel().getSelectedItem();
            for(Team te: draft.getTeams()){
                if(te.getName().equalsIgnoreCase(str)){
                    teamController.handleRemoveTeamRequest(this, te);
                    break;
                }
            }
            updateTeamSelect();
        });
        editTeamButton.setOnAction(e->{
            String str = (String) teamSelectComboBox.getSelectionModel().getSelectedItem();
            for(Team te: draft.getTeams()){
                if(te.getName().equalsIgnoreCase(str)){
                    teamController.handleEditTeamRequest(this, te);
                    break;
                }
            }
            updateTeamSelect();
        });
        startingLineup.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                
                Player si = (Player)startingLineup.getSelectionModel().getSelectedItem();
                playerController.handleEditPlayerRequest(this, si);
            }
        });
        
        autoPickButton.setOnAction(e -> {
            runThread = true;
            Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    if (runThread) {
                        for (Team te : draft.getTeams()) {
                            while (te.getHittersNeeded() + te.getPitchersNeeded() != 0) {
                                if (runThread) {
                                    if (!te.hasPosition("C")) {
                                        draftPlayer(te, "C");
                                    } else if (!te.hasPosition("1B")) {
                                        draftPlayer(te, "1B");
                                    } else if (!te.hasPosition("CI")) {
                                        Player p1 = draft.pickBestPlayerWithPos(players, "1B");
                                        Player p2 = draft.pickBestPlayerWithPos(players, "3B");
                                        ObservableList<Player> player = FXCollections.observableArrayList();
                                        player.add(p1);
                                        player.add(p2);
                                        Player pl = getBestPlayer(player);
                                        pl.setDraftTeam(te.getName());
                                        pl.setPosition("CI");
                                        pl.setContract("S2");
                                        pl.setSalary(1);
                                        pl.setTransactionNumber(draft.getTransactionNumber());
                                        draft.setTransactionNumber(draft.getTransactionNumber() + 1);
                                        players.remove(pl);
                                        reloadPlayersList(getActiveRadioButton());
                                        te.addPlayer(pl);
                                        reloadFantasyTeams();
                                        if (getTeamSelected() != null) {
                                            updateLineup(getTeamSelected());
                                        }
                                        addDraftPlayer(pl);
                                    } else if (!te.hasPosition("3B")) {
                                        draftPlayer(te, "3B");
                                    } else if (!te.hasPosition("2B")) {
                                        draftPlayer(te, "2B");
                                    } else if (!te.hasPosition("MI")) {
                                        Player p1 = draft.pickBestPlayerWithPos(players, "2B");
                                        Player p2 = draft.pickBestPlayerWithPos(players, "SS");
                                        ObservableList<Player> player = FXCollections.observableArrayList();
                                        player.add(p1);
                                        player.add(p2);
                                        Player pl = getBestPlayer(player);
                                        pl.setDraftTeam(te.getName());
                                        pl.setPosition("MI");
                                        pl.setContract("S2");
                                        pl.setSalary(1);
                                        pl.setTransactionNumber(draft.getTransactionNumber());
                                        draft.setTransactionNumber(draft.getTransactionNumber() + 1);
                                        players.remove(pl);
                                        reloadPlayersList(getActiveRadioButton());
                                        te.addPlayer(pl);
                                        reloadFantasyTeams();
                                        if (getTeamSelected() != null) {
                                            updateLineup(getTeamSelected());
                                        }
                                        addDraftPlayer(pl);
                                    } else if (!te.hasPosition("SS")) {
                                        draftPlayer(te, "SS");
                                    } else if (!te.hasPosition("OF")) {
                                        draftPlayer(te, "OF");
                                    } else if (!te.hasPosition("U")) {
                                        Player p1 = draft.pickBestPlayerWithPos(players, "C");
                                        Player p2 = draft.pickBestPlayerWithPos(players, "1B");
                                        Player p3 = draft.pickBestPlayerWithPos(players, "3B");
                                        Player p4 = draft.pickBestPlayerWithPos(players, "2B");
                                        Player p5 = draft.pickBestPlayerWithPos(players, "SS");
                                        Player p6 = draft.pickBestPlayerWithPos(players, "OF");
                                        ObservableList<Player> player = FXCollections.observableArrayList();
                                        player.add(p1);
                                        player.add(p2);
                                        player.add(p3);
                                        player.add(p4);
                                        player.add(p5);
                                        player.add(p6);
                                        Player pl = getBestPlayer(player);
                                        pl.setDraftTeam(te.getName());
                                        pl.setPosition("U");
                                        pl.setContract("S2");
                                        pl.setSalary(1);
                                        pl.setTransactionNumber(draft.getTransactionNumber());
                                        draft.setTransactionNumber(draft.getTransactionNumber() + 1);
                                        players.remove(pl);
                                        reloadPlayersList(getActiveRadioButton());
                                        te.addPlayer(pl);
                                        reloadFantasyTeams();
                                        if (getTeamSelected() != null) {
                                            updateLineup(getTeamSelected());
                                        }
                                        addDraftPlayer(pl);
                                    } else if (!te.hasPosition("P")) {
                                        draftPlayer(te, "P");
                                    }
                                }
                                Thread.sleep(100);
                            }
                        }
                        for (Team te : draft.getTeams()) {
                            int s = te.getTaxiSize();
                            while (te.getTaxiSize() < 8) {
                                if (runThread) {
                                    Player pl = draft.pickBestTaxiPlayer(players);
                                    pl.setDraftTeam(te.getName());
                                    players.remove(pl);
                                    reloadPlayersList(getActiveRadioButton());
                                    te.addPlayer(pl);
                                    reloadFantasyTeams();
                                    if (getTeamSelected() != null) {
                                        updateLineup(getTeamSelected());
                                    }
                                }

                            }

                        }

                    }
                    return null;
                }

            };
        thread = new Thread(task);
        thread.start();
        });
        
        pauseButton.setOnAction((ActionEvent e)->{
            runThread = false;
        });
        
        pickButton.setOnAction((ActionEvent e)->{
            Team te = null;
            for(Team t: draft.getTeams()){
                if((t.getHittersNeeded() + t.getPitchersNeeded() != 0)){
                    te = t;
                    break;
                }
            }
            if(te != null){
                if(!te.hasPosition("C")){
                    draftPlayer(te, "C");
                }
                else if(!te.hasPosition("1B")){
                    draftPlayer(te, "1B");
                }
                else if(!te.hasPosition("CI")){
                    Player p1 = draft.pickBestPlayerWithPos(players, "1B");
                    Player p2 = draft.pickBestPlayerWithPos(players, "3B");
                    ObservableList<Player> player = FXCollections.observableArrayList();
                    player.add(p1);
                    player.add(p2);
                    Player pl = getBestPlayer(player);
                    pl.setDraftTeam(te.getName());
                    pl.setPosition("CI");
                    pl.setContract("S2");
                    pl.setSalary(1);
                    pl.setTransactionNumber(draft.getTransactionNumber());
                    draft.setTransactionNumber(draft.getTransactionNumber() + 1);
                    players.remove(pl);
                    reloadPlayersList(getActiveRadioButton());
                    te.addPlayer(pl);
                    reloadFantasyTeams();
                    if (getTeamSelected() != null) {
                        updateLineup(getTeamSelected());
                    }
                    addDraftPlayer(pl);
                }
                else if(!te.hasPosition("3B")){
                    draftPlayer(te, "3B");
                }
                else if(!te.hasPosition("2B")){
                    draftPlayer(te, "2B");
                }
                else if(!te.hasPosition("MI")){
                    Player p1 = draft.pickBestPlayerWithPos(players, "2B");
                    Player p2 = draft.pickBestPlayerWithPos(players, "SS");
                    ObservableList<Player> player = FXCollections.observableArrayList();
                    player.add(p1);
                    player.add(p2);
                    Player pl = getBestPlayer(player);
                    pl.setDraftTeam(te.getName());
                    pl.setPosition("MI");
                    pl.setContract("S2");
                    pl.setSalary(1);
                    pl.setTransactionNumber(draft.getTransactionNumber());
                    draft.setTransactionNumber(draft.getTransactionNumber() + 1);
                    players.remove(pl);
                    reloadPlayersList(getActiveRadioButton());
                    te.addPlayer(pl);
                    reloadFantasyTeams();
                    if (getTeamSelected() != null) {
                        updateLineup(getTeamSelected());
                    }
                    addDraftPlayer(pl);
                }
                else if(!te.hasPosition("SS")){
                    draftPlayer(te, "SS");
                }
                else if(!te.hasPosition("OF")){
                    draftPlayer(te, "OF");
                }
                else if(!te.hasPosition("U")){
                    Player p1 = draft.pickBestPlayerWithPos(players, "C");
                    Player p2 = draft.pickBestPlayerWithPos(players, "1B");
                    Player p3 = draft.pickBestPlayerWithPos(players, "3B");
                    Player p4 = draft.pickBestPlayerWithPos(players, "2B");
                    Player p5 = draft.pickBestPlayerWithPos(players, "SS");
                    Player p6 = draft.pickBestPlayerWithPos(players, "OF");
                    ObservableList<Player> player = FXCollections.observableArrayList();
                    player.add(p1);
                    player.add(p2);
                    player.add(p3);
                    player.add(p4);
                    player.add(p5);
                    player.add(p6);
                    Player pl = getBestPlayer(player);
                    pl.setDraftTeam(te.getName());
                    pl.setPosition("U");
                    pl.setContract("S2");
                    pl.setSalary(1);
                    pl.setTransactionNumber(draft.getTransactionNumber());
                    draft.setTransactionNumber(draft.getTransactionNumber() + 1);
                    players.remove(pl);
                    reloadPlayersList(getActiveRadioButton());
                    te.addPlayer(pl);
                    reloadFantasyTeams();
                    if (getTeamSelected() != null) {
                        updateLineup(getTeamSelected());
                    }
                    addDraftPlayer(pl);
                }
                else if(!te.hasPosition("P")){
                    draftPlayer(te, "P");
                }
            }
            else{
                for(Team t: draft.getTeams()){
                    if(t.getTaxiSize() < 8){
                        te = t;
                        break;
                    }
                }
                if(te != null){
                    Player pl = draft.pickBestTaxiPlayer(players);
                    pl.setDraftTeam(te.getName());
                    players.remove(pl);
                    reloadPlayersList(getActiveRadioButton());
                    te.addPlayer(pl);
                    reloadFantasyTeams();
                    if (getTeamSelected() != null) {
                        updateLineup(getTeamSelected());
                    }
                }
            }
            ObservableList<Player> fullPlayers = FXCollections.observableArrayList();
            for(Team tea: draft.getTeams()){
                for(Player pla: tea.getPlayers()){
                    fullPlayers.add(pla);
                }
            }
            for(Player pla: players){
                fullPlayers.add(pla);
            }
            draft.givePlayerValues(fullPlayers);
            draft.giveTeamPoints();
        });
    }

    private Player getBestPlayer(ObservableList<Player> playersThings){
        Player bestPlayer = new Player();
        for(Player pl:playersThings){
            if(pl.getBAWHIPS() >= bestPlayer.getBAWHIPS()){
                bestPlayer = pl;
            }
        }
        return bestPlayer;
    }
    
    private void draftPlayer(Team te, String str){
        Player pl = draft.pickBestPlayerWithPos(players, str);
        pl.setDraftTeam(te.getName());
        pl.setPosition(str);
        pl.setContract("S2");
        pl.setSalary(1);
        pl.setTransactionNumber(draft.getTransactionNumber());
        draft.setTransactionNumber(draft.getTransactionNumber() + 1);
        players.remove(pl);
        reloadPlayersList(getActiveRadioButton());
        te.addPlayer(pl);
        reloadFantasyTeams();
        if (getTeamSelected() != null) {
            updateLineup(getTeamSelected());
        }
        addDraftPlayer(pl);
    } 
    
    private void registerSearchField(TextField textField){
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            TextFieldContent = textField.getText();
            reloadPlayersList(getActiveRadioButton());
        });
    }
    
    public int getActiveRadioButton(){
        if(all) return 0;
        else if(C) return 1;
        else if(B1) return 2;
        else if(CI) return 3;
        else if(B3) return 4;
        else if(B2) return 5;
        else if(MI) return 6;
        else if(SS) return 7;
        else if(OF) return 8;
        else if(U) return 9;
        else return 10;
    }
    
    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(Pane toolbar, CSB_PropertyType icon, CSB_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initLabel(CSB_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }

    // INIT A LABEL AND PLACE IT IN A GridPane INIT ITS PROPER PLACE
    private Label initGridLabel(GridPane container, CSB_PropertyType labelProperty, String styleClass, int col, int row, int colSpan, int rowSpan) {
        Label label = initLabel(labelProperty, styleClass);
        container.add(label, col, row, colSpan, rowSpan);
        return label;
    }

    // INIT A LABEL AND PUT IT IN A TOOLBAR
    private Label initChildLabel(Pane container, CSB_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }

    // INIT A COMBO BOX AND PUT IT IN A GridPane
    private ComboBox initGridComboBox(GridPane container, int col, int row, int colSpan, int rowSpan) throws IOException {
        ComboBox comboBox = new ComboBox();
        container.add(comboBox, col, row, colSpan, rowSpan);
        return comboBox;
    }

    

    // INIT A TEXT FIELD AND PUT IT IN A GridPane
    private TextField initGridTextField(GridPane container, int size, String initText, boolean editable, int col, int row, int colSpan, int rowSpan) {
        TextField tf = new TextField();
        tf.setPrefColumnCount(size);
        tf.setText(initText);
        tf.setEditable(editable);
        container.add(tf, col, row, colSpan, rowSpan);
        return tf;
    }

    // INIT A DatePicker AND PUT IT IN A GridPane
    private DatePicker initGridDatePicker(GridPane container, int col, int row, int colSpan, int rowSpan) {
        DatePicker datePicker = new DatePicker();
        container.add(datePicker, col, row, colSpan, rowSpan);
        return datePicker;
    }

    // INIT A CheckBox AND PUT IT IN A TOOLBAR
    private CheckBox initChildCheckBox(Pane container, String text) {
        CheckBox cB = new CheckBox(text);
        container.getChildren().add(cB);
        return cB;
    }

    // INIT A DatePicker AND PUT IT IN A CONTAINER
    private DatePicker initChildDatePicker(Pane container) {
        DatePicker dp = new DatePicker();
        container.getChildren().add(dp);
        return dp;
    }
    
    // LOADS CHECKBOX DATA INTO A Course OBJECT REPRESENTING A CoursePage
    private void updatePageUsingCheckBox(CheckBox cB, Course course, CoursePage cP) {
        if (cB.isSelected()) {
            course.selectPage(cP);
        } else {
            course.unselectPage(cP);
        }
    }    
}
