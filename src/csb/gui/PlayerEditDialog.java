package csb.gui;

import csb.CSB_PropertyType;
import csb.data.Draft;
import csb.data.Player;
import csb.data.Team;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import java.util.Arrays;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
/**
 *
 * @author Ethan
 */
public class PlayerEditDialog extends Stage{
    // THIS IS THE OBJECT DATA BEHIND THIS UI
    Player player;
    Team selectedTeam;
    boolean bl;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    ImageView playerPicture;
    ImageView countryPicture;
    Label nameLabel;
    Label positionsLabel;
    Label headingLabel;
    Label fTeamLabel;
    Label positionLabel;
    Label contractLabel;
    Label salaryLabel;
    TextField salaryTextField;
    ComboBox fTeamComboBox;
    ComboBox positionComboBox;
    ComboBox contractComboBox;
    Button completeButton;
    Button cancelButton;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String FTEAM_PROMPT = "Fantasy Team: ";
    public static final String POSITION_PROMPT = "Position: ";
    public static final String CONTRACT_PROMPT = "Contract: ";
    public static final String SALARY_PROMPT = "Salary($): ";
    public static final String EDIT_PLAYER_HEADING = "Player Details";
    public static final String EDIT_PLAYER_TITLE = "Edit Player";
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public PlayerEditDialog(Stage primaryStage, Draft draft, MessageDialog messageDialog) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        player = new Player();
        playerPicture = new ImageView();
        countryPicture = new ImageView();
        headingLabel = new Label(EDIT_PLAYER_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        nameLabel = new Label();
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        nameLabel.setWrapText(true);
        positionsLabel = new Label();
        positionsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        positionsLabel.setWrapText(true);
        // NOW THE DESCRIPTION 
        fTeamLabel = new Label(FTEAM_PROMPT);
        fTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fTeamComboBox = new ComboBox();
        updateTeams(draft);
        fTeamComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            player.setDraftTeam((String) newValue);
            for(Team te: draft.getTeams()){
                if(te.getName().equalsIgnoreCase(newValue.toString())){
                    selectedTeam = te;
                    loadGUIData(draft);
                    break;
                }
            }
        });
        
        positionLabel = new Label(POSITION_PROMPT);
        positionLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        positionComboBox = new ComboBox();
        
        positionComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            try{
                String str = newValue.toString();
                SimpleStringProperty ssp = new SimpleStringProperty(str);
                player.setTeamPositionProperty(ssp);
            }
            catch(NullPointerException e){
                //This happens when the position select combo box's
                //value is forcibly changed to null by changing the 
                //selected team, it can be ignored.
            }
        });
        
        contractLabel = new Label(CONTRACT_PROMPT);
        contractLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        contractComboBox = new ComboBox();
        ObservableList<String> contracts = FXCollections.observableArrayList();
        contracts.add("S1");
        contracts.add("S2");
        contracts.add("X");
        contractComboBox.setItems(contracts);
        contractComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            player.setContract((String) newValue);
        });
        
        salaryLabel = new Label(SALARY_PROMPT);
        salaryTextField = new TextField();
        salaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            try  
            {  
                player.setSalary(Integer.parseInt(newValue));
            }  
            catch(NumberFormatException nfe)  
            {  
            }  
        });
        
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler cancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            PlayerEditDialog.this.selection = sourceButton.getText();
            PlayerEditDialog.this.hide();
        };
        EventHandler completeHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            if(fTeamComboBox.getValue().equals("FREE_AGENTS")){
                Button sourceButton = (Button)ae.getSource();
                PlayerEditDialog.this.selection = sourceButton.getText();
                PlayerEditDialog.this.hide();
            }
            else if(fTeamComboBox.getValue() == null || positionComboBox.getValue() == null || contractComboBox.getValue() == null || Integer.parseInt(salaryTextField.getText()) < 1){
                boolean filled = false;
                for(Team te: draft.getTeams()){
                    if(te.getHittersNeeded() + te.getPitchersNeeded() == 0){
                        filled = true;
                    }
                    else{
                        filled = false;
                        break;
                    }
                }
                if(filled){
                    Button sourceButton = (Button)ae.getSource();
                    PlayerEditDialog.this.selection = sourceButton.getText();
                    PlayerEditDialog.this.hide();
                }
            }
            else{
                Button sourceButton = (Button)ae.getSource();
                PlayerEditDialog.this.selection = sourceButton.getText();
                PlayerEditDialog.this.hide();
            }
        };
        completeButton.setOnAction(completeHandler);
        cancelButton.setOnAction(cancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(playerPicture, 0, 1, 2, 3);
        gridPane.add(countryPicture, 1, 1, 1, 1);
        gridPane.add(nameLabel, 1, 2, 1, 1);
        gridPane.add(positionsLabel, 1, 3, 1, 1);
        gridPane.add(fTeamLabel, 0, 4, 1, 1);
        gridPane.add(fTeamComboBox, 1, 4, 1, 1);
        gridPane.add(positionLabel, 0, 5, 1, 1);
        gridPane.add(positionComboBox, 1, 5, 1, 1);
        gridPane.add(contractLabel, 0, 6, 1, 1);
        gridPane.add(contractComboBox, 1, 6, 1, 1);
        gridPane.add(salaryLabel, 0, 7, 1, 1);
        gridPane.add(salaryTextField, 1, 7, 1, 1);
        gridPane.add(completeButton, 0, 8, 1, 1);
        gridPane.add(cancelButton, 1, 8, 1, 1);
        
        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public Player getPlayer() { 
        return player;
    }
    
    public final void updateTeams(Draft draft) { 
        ObservableList<String> teams = FXCollections.observableArrayList();
        teams.add("FREE_AGENTS");
        for(int i = 0; i < draft.getTeams().size(); i++){
            teams.add(draft.getTeams().get(i).getName());
        }
        fTeamComboBox.setItems(teams);
    }
    
    public Player showEditPlayerDialog(Player playerToUse, Draft draft) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_PLAYER_TITLE);
        
        // RESET THE PLAYER OBJECT WITH DEFAULT VALUES
        player.setPositions(playerToUse.getPositions());
        player.setDraftTeam(playerToUse.getDraftTeam());
        player.setSalary(playerToUse.getSalary());
        player.setContract(playerToUse.getContract());
        player.setFirstName(playerToUse.getFirstName());
        player.setLastName(playerToUse.getLastName());
        player.setCountry(playerToUse.getCountry());
        
        // LOAD THE UI STUFF
        salaryTextField.setText(String.valueOf(player.getSalary()));
        
        
        // AND OPEN IT UP
        
        loadGUIData(draft);
        this.showAndWait();
        
        return player;
    }
    
    public final void loadGUIData(Draft draft) {
        // LOAD THE UI STUFF
        salaryTextField.setText(String.valueOf(player.getSalary()));
        fTeamComboBox.setValue(player.getDraftTeam());
        positionComboBox.setValue(null);
        contractComboBox.setValue(null);
        positionsLabel.setText(player.getPositions());
        nameLabel.setText(Character.toString(player.getFirstName().charAt(0)).toUpperCase()+player.getFirstName().substring(1).toLowerCase() + " " + 
                Character.toString(player.getLastName().charAt(0)).toUpperCase()+player.getLastName().substring(1).toLowerCase());
        String playerName = "csb/gui/players/" + Character.toString(player.getLastName().charAt(0)).toUpperCase()+player.getLastName().substring(1).toLowerCase() + 
                Character.toString(player.getFirstName().charAt(0)).toUpperCase()+player.getFirstName().substring(1).toLowerCase() + ".jpg";
        try{
            Image playerImage = new Image(playerName);
            playerPicture.setImage(playerImage);
        }
        catch(Exception e){
            Image playerImage = new Image("csb/gui/players/AAA_PhotoMissing.jpg");
            playerPicture.setImage(playerImage);
        }
            String countryName = "csb/gui/flags/" + player.getCountry() + ".png";
        try{
            Image countryImage = new Image(countryName);
            countryPicture.setImage(countryImage);
        }
        catch(Exception e){
        }
        ObservableList<String> positions = FXCollections.observableArrayList();
        String playerPos;
        int cCount = 0;
        int b1Count = 0;
        int b3Count = 0;
        int b2Count = 0;
        int ssCount = 0;
        int ofCount = 0;
        int pCount = 0;
        int uCount = 0;
        int ciCount = 0;
        int miCount = 0;
        if(player.getPositions() != null){
            playerPos = player.getPositions();
            if(player.getPosition() == null){
                player.setPosition("");
            }
            if(selectedTeam != null && !fTeamComboBox.getValue().toString().equals("FREE_AGENTS")){
                for(Player pl: selectedTeam.getPlayers()){
                    switch(pl.getPosition()){
                        case "C": cCount++;
                            break;
                        case "1B": b1Count++;
                            break;
                        case "3B": b3Count++;
                            break;
                        case "2B": b2Count++;
                            break;
                        case "SS": ssCount++;
                            break;
                        case "OF": ofCount++;
                            break;
                        case "P": pCount++;
                            break;    
                        case "U": uCount++;
                            break;   
                        case "CI": ciCount++;
                            break;   
                        case "MI": miCount++;
                            break;   
                        default: break;
                    }
                }
            }
        }
        else playerPos = "";
        if(playerPos.contains("C")){
            if(cCount < 2){
                positions.add("C");
            }
            if(!positions.contains("U") && uCount < 1){
                positions.add("U");
            }
        }
        if(playerPos.contains("1B")){
            if(b1Count < 1){
                positions.add("1B");
            }
            if(!positions.contains("U") && uCount < 1){
                positions.add("U");
            }
            if(!positions.contains("CI") && ciCount < 1){
                positions.add("CI");
            }
        }
        if(playerPos.contains("3B")){
            if(b3Count < 1){
                positions.add("3B");
            }
            if(!positions.contains("U") && uCount < 1){
                positions.add("U");
            }
            if(!positions.contains("CI") && ciCount < 1){
                positions.add("CI");
            }
        }
        if(playerPos.contains("2B")){
            if(b2Count < 1){
                positions.add("2B");
            }
            if(!positions.contains("U") && uCount < 1){
                positions.add("U");
            }
            if(!positions.contains("MI") && miCount < 1){
                positions.add("MI");
            }
        }
        if(playerPos.contains("SS")){
            if(ssCount < 1){
                positions.add("SS");
            }
            if(!positions.contains("U") && uCount < 1){
                positions.add("U");
            }
            if(!positions.contains("MI") && miCount < 1){
                positions.add("MI");
            }
        }
        if(playerPos.contains("OF")){
            if(ofCount < 5){
                positions.add("OF");
            }
            if(!positions.contains("U") && uCount < 1){
                positions.add("U");
            }
        }
        if(playerPos.contains("P") && pCount < 9){
            positions.add("P");
        }
        positionComboBox.setItems(positions);
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    
}
