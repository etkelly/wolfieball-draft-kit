package csb.gui;

import csb.CSB_PropertyType;
import csb.data.Draft;
import csb.data.Player;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import java.util.Arrays;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;



public class PlayerAddDialog extends Stage{
    // THIS IS THE OBJECT DATA BEHIND THIS UI
    Player player;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    HBox checkHBox;
    Label headingLabel;
    Label fNameLabel;
    Label lNameLabel;
    Label proTeamLabel;
    TextField fNameTextField;
    TextField lNameTextField;
    ComboBox proTeamComboBox;
    Label CLabel; 
    Label B1Label; 
    Label B3Label; 
    Label B2Label; 
    Label SSLabel; 
    Label OFLabel; 
    Label PLabel; 
    CheckBox CCheckBox; 
    CheckBox B1CheckBox; 
    CheckBox B3CheckBox; 
    CheckBox B2CheckBox; 
    CheckBox SSCheckBox; 
    CheckBox OFCheckBox; 
    CheckBox PCheckBox; 
    Button completeButton;
    Button cancelButton;
    boolean hasPosition;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String FNAME_PROMPT = "First Name: ";
    public static final String LNAME_PROMPT = "Last Name: ";
    public static final String PRO_TEAM_PROMPT = "Pro Team: ";
    public static final String ADD_PLAYER_HEADING = "Player Details";
    public static final String ADD_PLAYER_TITLE = "Add a new Player";
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public PlayerAddDialog(Stage primaryStage, Draft draft, MessageDialog messageDialog) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        hasPosition = false;
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        player = new Player();
        headingLabel = new Label(ADD_PLAYER_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        fNameLabel = new Label(FNAME_PROMPT);
        fNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fNameTextField = new TextField();
        fNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setFirstName(newValue);
        });
        
        lNameLabel = new Label(LNAME_PROMPT);
        lNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        lNameTextField = new TextField();
        lNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setLastName(newValue);
        });
        
        proTeamLabel = new Label(PRO_TEAM_PROMPT);
        proTeamComboBox = new ComboBox();
        ObservableList<String> proTeams = FXCollections.observableArrayList();
        proTeams.add("ATL");
        proTeams.add("AZ");
        proTeams.add("CHC");
        proTeams.add("CIN");
        proTeams.add("COL");
        proTeams.add("LAD");
        proTeams.add("MIA");
        proTeams.add("MIL");
        proTeams.add("NYM");
        proTeams.add("PHI");
        proTeams.add("PIT");
        proTeams.add("SD");
        proTeams.add("SF");
        proTeams.add("STL");
        proTeams.add("WAS");
        proTeamComboBox.setItems(proTeams);
        proTeamComboBox.setValue("ATL");
        proTeamComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            player.setProTeam((String) newValue);
        });
        
        CCheckBox= new CheckBox();
        CLabel = new Label("C");
        CCheckBox.setOnAction(e -> {
            updatePlayerUsingCheckBox();
        });
        B1Label = new Label("1B");
        B1CheckBox= new CheckBox();
        B1CheckBox.setOnAction(e -> {
            updatePlayerUsingCheckBox();
        });
        B3Label = new Label("3B");
        B3CheckBox= new CheckBox();
        B3CheckBox.setOnAction(e -> {
            updatePlayerUsingCheckBox();
        });
        B2Label = new Label("2B");
        B2CheckBox= new CheckBox();
        B2CheckBox.setOnAction(e -> {
            updatePlayerUsingCheckBox();
        });
        SSLabel = new Label("SS");
        SSCheckBox= new CheckBox();
        SSCheckBox.setOnAction(e -> {
            updatePlayerUsingCheckBox();
        });
        OFLabel = new Label("OF");
        OFCheckBox= new CheckBox();
        OFCheckBox.setOnAction(e -> {
            updatePlayerUsingCheckBox();
        });
        PLabel = new Label("P");
        PCheckBox= new CheckBox();
        PCheckBox.setOnAction(e -> {
            updatePlayerUsingCheckBox();
        });
        checkHBox = new HBox();
        checkHBox.getChildren().add(CLabel);
        checkHBox.getChildren().add(CCheckBox);
        checkHBox.getChildren().add(B1Label);
        checkHBox.getChildren().add(B1CheckBox);
        checkHBox.getChildren().add(B3Label);
        checkHBox.getChildren().add(B3CheckBox);
        checkHBox.getChildren().add(B2Label);
        checkHBox.getChildren().add(B2CheckBox);
        checkHBox.getChildren().add(SSLabel);
        checkHBox.getChildren().add(SSCheckBox);
        checkHBox.getChildren().add(OFLabel);
        checkHBox.getChildren().add(OFCheckBox);
        checkHBox.getChildren().add(PLabel);
        checkHBox.getChildren().add(PCheckBox);
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler cancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            PlayerAddDialog.this.selection = sourceButton.getText();PlayerAddDialog.this.hide();
            PlayerAddDialog.this.hide();
        };
        EventHandler completeHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            PlayerAddDialog.this.selection = sourceButton.getText();
            if(player.getFirstName() == null || player.getLastName() == null || player.getPositions() == null){
                
            }
            else if(!player.getFirstName().equals("") && !player.getLastName().equals("") && !player.getPositions().equals("")){
                PlayerAddDialog.this.hide();
            }
        };
        completeButton.setOnAction(completeHandler);
        cancelButton.setOnAction(cancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(fNameLabel, 0, 1, 1, 1);
        gridPane.add(lNameLabel, 0, 2, 1, 1);
        gridPane.add(proTeamLabel, 0, 3, 1, 1);
        gridPane.add(fNameTextField, 1, 1, 1, 1);
        gridPane.add(lNameTextField, 1, 2, 1, 1);
        gridPane.add(proTeamComboBox, 1, 3, 1, 1);
        gridPane.add(checkHBox, 0, 4, 4, 2);
        gridPane.add(completeButton, 0, 7, 1, 1);
        gridPane.add(cancelButton, 1, 7, 1, 1);
        
        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public Player getPlayer() { 
        return player;
    }
    
    private void updatePlayerUsingCheckBox(){
        String playerPos = "";
        if(CCheckBox.isSelected()){
            if(playerPos.equals("")){
                playerPos += "C"; 
            }
            else playerPos += "_C";
        }
        if(B1CheckBox.isSelected()){
            if(playerPos.equals("")){
                playerPos += "1B"; 
            }
            else playerPos += "_1B";
        }
        if(B3CheckBox.isSelected()){
            if(playerPos.equals("")){
                playerPos += "3B"; 
            }
            else playerPos += "_3B";
        }
        if(B2CheckBox.isSelected()){
            if(playerPos.equals("")){
                playerPos += "2B"; 
            }
            else playerPos += "_2B";
        }
        if(SSCheckBox.isSelected()){
            if(playerPos.equals("")){
                playerPos += "SS"; 
            }
            else playerPos += "_SS";
        }
        if(OFCheckBox.isSelected()){
            if(playerPos.equals("")){
                playerPos += "OF"; 
            }
            else playerPos += "_OF";
        }
        if(PCheckBox.isSelected()){
            if(playerPos.equals("")){
                playerPos += "P"; 
            }
            else playerPos += "_P";
        }
        player.setPositions(playerPos);
    }
            
    
    public Player showAddPlayerDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_PLAYER_TITLE);
        
        player = new Player();
        player.setProTeam("ATL");
        
        
        // AND OPEN IT UP
        
        loadGUIData();
        this.showAndWait();
        
        return player;
    }
    
    public void loadGUIData() {
        
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
}
