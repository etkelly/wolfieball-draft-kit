
package csb.controller;
import csb.data.CourseDataManager;
import csb.data.Draft;
import csb.data.Player;
import csb.data.Team;
import csb.gui.TeamAddDialog;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.YesNoCancelDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
/**
 *
 * @author Ethan
 */
public class TeamEditController {
    TeamAddDialog tad;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public TeamEditController(Stage initPrimaryStage, Draft draft, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        tad = new TeamAddDialog(initPrimaryStage, draft, initMessageDialog);
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    // THESE ARE FOR SCHEDULE ITEMS
    
    public void handleAddTeamRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Draft draft = cdm.getDraft();
        tad.showAddTeamDialog();
        
        // DID THE USER CONFIRM?
        if (tad.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            Team t = tad.getTeam();
            
            // AND ADD IT AS A ROW TO THE TABLE
            draft.addTeam(t);
            gui.reloadFantasyTeams();
            ObservableList<Player> fullPlayers = FXCollections.observableArrayList();
            for(Team tea: gui.getDataManager().getDraft().getTeams()){
                for(Player pla: tea.getPlayers()){
                    fullPlayers.add(pla);
                }
            }
            for(Player pla: gui.getPlayerList()){
                fullPlayers.add(pla);
            }
            gui.getDataManager().getDraft().givePlayerValues(fullPlayers);
            gui.getDataManager().getDraft().giveTeamPoints();
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            gui.getFileController().markAsEdited(gui);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditTeamRequest(CSB_GUI gui, Team itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Draft draft = cdm.getDraft();
        tad.showEditTeamDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (tad.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            Team te = tad.getTeam();
            itemToEdit.setName(te.getName());
            itemToEdit.setOwner(te.getOwner());
            
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            gui.getFileController().markAsEdited(gui);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveTeamRequest(CSB_GUI gui, Team itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show("Remove this Team?");
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN REMOVE IT
        if (selection.equals(YesNoCancelDialog.YES)) { 
            for(Player pl: itemToRemove.getPlayers()){
                pl.setDraftTeam("FREE_AGENTS");
                gui.getPlayerList().add(pl);
            }
            itemToRemove.getPlayers().clear();
            gui.getDataManager().getDraft().removeTeam(itemToRemove);
            gui.getDataManager().getDraft().giveTeamPoints();
            gui.reloadFantasyTeams();
            ObservableList<Player> fullPlayers = FXCollections.observableArrayList();
            for(Team t: gui.getDataManager().getDraft().getTeams()){
                for(Player pla: t.getPlayers()){
                    fullPlayers.add(pla);
                }
            }
            for(Player pla: gui.getPlayerList()){
                fullPlayers.add(pla);
            }
            gui.getDataManager().getDraft().givePlayerValues(fullPlayers);
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            gui.getFileController().markAsEdited(gui);
        }
    }
}
