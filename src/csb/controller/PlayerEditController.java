/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Draft;
import csb.data.Player;
import csb.data.Team;
import csb.gui.PlayerEditDialog;
import csb.gui.PlayerAddDialog;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.YesNoCancelDialog;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

public class PlayerEditController {

    PlayerEditDialog ped;
    PlayerAddDialog pad;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    Draft draftToUse;

    public PlayerEditController(Stage initPrimaryStage, Player player, Draft draft, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        ped = new PlayerEditDialog(initPrimaryStage, draft, initMessageDialog);
        pad = new PlayerAddDialog(initPrimaryStage, draft, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
        draftToUse = draft;
    }

    public void handleEditPlayerRequest(CSB_GUI gui, Player itemToEdit) {
        ped.updateTeams(draftToUse);
        Team tea = null;
        if (itemToEdit.getDraftTeam() != null) {
            for (Team t : gui.getDataManager().getDraft().getTeams()) {
                if (t.getName().equalsIgnoreCase(itemToEdit.getDraftTeam())) {
                    tea = t;
                    break;
                }
            }
        }
        ped.showEditPlayerDialog(itemToEdit, gui.getDataManager().getDraft());
        // DID THE USER CONFIRM?
        if (ped.wasCompleteSelected()) {
            // UPDATE THE PLAYER
            boolean full = false;
            String oldContract = "";
            Player pl = ped.getPlayer();
            itemToEdit.setDraftTeam(pl.getDraftTeam());
            oldContract = itemToEdit.getContract();
            for (Team te : draftToUse.getTeams()) {
                if (te.getName().equalsIgnoreCase(pl.getDraftTeam())) {
                    if (te.getTaxiSize() > 7 && (te.getHittersNeeded() + te.getPitchersNeeded() == 0)) {
                        itemToEdit.setDraftTeam("FREE_AGENTS");
                        full = true;
                    } else if (te.getHittersNeeded() + te.getPitchersNeeded() > 0){
                        SimpleStringProperty ssp = new SimpleStringProperty(pl.getPosition());
                        itemToEdit.setTeamPositionProperty(ssp);
                        itemToEdit.setContract(pl.getContract());
                        itemToEdit.setSalary(pl.getSalary());
                    } else {
                        itemToEdit.setContract("X");
                    }
                }
            }
            if (!full) {
                if (!itemToEdit.getDraftTeam().equals("FREE_AGENTS")) {
                    if (tea == null) {
                        if (itemToEdit.getContract().equalsIgnoreCase("S2")) {
                            itemToEdit.setTransactionNumber(draftToUse.getTransactionNumber());
                            draftToUse.setTransactionNumber(draftToUse.getTransactionNumber() + 1);
                        }
                        gui.getPlayerList().remove(itemToEdit);
                        gui.reloadPlayersList(gui.getActiveRadioButton());
                        for (Team te : draftToUse.getTeams()) {
                            if (te.getName().equalsIgnoreCase(itemToEdit.getDraftTeam())) {
                                te.addPlayer(itemToEdit);
                                te.setStats();
                                gui.reloadFantasyTeams();
                                if (gui.getTeamSelected() != null) {
                                    gui.updateLineup(gui.getTeamSelected());
                                }
                                if (itemToEdit.getContract().equalsIgnoreCase("S2")) {
                                    gui.addDraftPlayer(itemToEdit);
                                }
                                break;
                            }
                        }
                    } else {
                        if (itemToEdit.getContract().equalsIgnoreCase("S2")) {
                            draftToUse.setTransactionNumber(draftToUse.getTransactionNumber() - 1);
                            itemToEdit.setTransactionNumber(draftToUse.getTransactionNumber());
                            draftToUse.setTransactionNumber(draftToUse.getTransactionNumber() + 1);
                        }
                        tea.removePlayer(itemToEdit);
                        tea.setStats();
                        for (Team te : draftToUse.getTeams()) {
                            if (te.getName().equalsIgnoreCase(itemToEdit.getDraftTeam())) {
                                te.addPlayer(itemToEdit);
                                te.setStats();
                                gui.reloadFantasyTeams();
                                if (gui.getTeamSelected() != null) {
                                    gui.updateLineup(gui.getTeamSelected());
                                }
                                if (oldContract.equalsIgnoreCase("S2")) {
                                    gui.removeDraftPlayer(itemToEdit);
                                    if (itemToEdit.getContract().equalsIgnoreCase("S2")) {
                                        gui.addDraftPlayer(itemToEdit);
                                    } else {
                                        draftToUse.setTransactionNumber(draftToUse.getTransactionNumber() - 1);
                                    }
                                } else {
                                    if (itemToEdit.getContract().equalsIgnoreCase("S2")) {
                                        itemToEdit.setTransactionNumber(draftToUse.getTransactionNumber());
                                        draftToUse.setTransactionNumber(draftToUse.getTransactionNumber() + 1);
                                        gui.addDraftPlayer(itemToEdit);
                                    }
                                }
                                break;
                            }
                        }
                    }
                } else {
                    for (Team te : draftToUse.getTeams()) {
                        for (Player pla : te.getPlayers()) {
                            if (pla.getFirstName().equalsIgnoreCase(itemToEdit.getFirstName()) && pla.getLastName().equalsIgnoreCase(itemToEdit.getLastName())) {
                                te.removePlayer(itemToEdit);
                                te.setStats();
                                gui.reloadFantasyTeams();
                                if (gui.getTeamSelected() != null) {
                                    gui.updateLineup(gui.getTeamSelected());
                                }
                                gui.getPlayerList().add(itemToEdit);
                                if (oldContract.equalsIgnoreCase("S2")) {
                                    draftToUse.setTransactionNumber(draftToUse.getTransactionNumber() - 1);
                                    gui.removeDraftPlayer(itemToEdit);
                                }
                                break;
                            }
                        }
                        for (Player pla : te.getTaxi()) {
                            if (pla.getFirstName().equalsIgnoreCase(itemToEdit.getFirstName()) && pla.getLastName().equalsIgnoreCase(itemToEdit.getLastName())) {
                                te.removePlayer(itemToEdit);
                                te.setStats();
                                gui.reloadFantasyTeams();
                                if (gui.getTeamSelected() != null) {
                                    gui.updateLineup(gui.getTeamSelected());
                                }
                                gui.getPlayerList().add(itemToEdit);
                                break;
                            }
                        }
                    }
                }
            }
            ObservableList<Player> fullPlayers = FXCollections.observableArrayList();
            for(Team t: gui.getDataManager().getDraft().getTeams()){
                for(Player pla: t.getPlayers()){
                    fullPlayers.add(pla);
                }
            }
            for(Player pla: gui.getPlayerList()){
                fullPlayers.add(pla);
            }
            gui.getDataManager().getDraft().givePlayerValues(fullPlayers);
            gui.getDataManager().getDraft().giveTeamPoints();

            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            gui.getFileController().markAsEdited(gui);
        } else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }

    public void handleRemovePlayerRequest(CSB_GUI gui, Player itemToEdit) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));

        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN REMOVE IT
        if (selection.equals(YesNoCancelDialog.YES)) {
            gui.getPlayerList().remove(itemToEdit);
            gui.getViewablePlayerList().remove(itemToEdit);
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            gui.getFileController().markAsEdited(gui);
        }
    }

    public void handleAddPlayerRequest(CSB_GUI gui) {
        pad.showAddPlayerDialog();

        // DID THE USER CONFIRM?
        if (pad.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            Player pl = pad.getPlayer();
            gui.getPlayerList().add(pl);
            gui.getFileController().markAsEdited(gui);

        } else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
}
